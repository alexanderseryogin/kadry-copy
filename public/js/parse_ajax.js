$(function () {
    $('.btn.btn-primary.pull-right').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: $(this).parents().find('form:visible').attr('action'),
            data: $(this).parents().find('form:visible').serialize(),
            success: function (response) {
                console.log(response);
            }
        });
    });
});
//# sourceMappingURL=parse_ajax.js.map
