$(function () {
    $('#upload').on('click', function () {
        $('input[name=cv]').trigger('click');
    });

    $('input[name=cv]').on('change', function () {
        $(this).parents().find('form:first').submit();
    });

});

//# sourceMappingURL=upload_cv.js.map
