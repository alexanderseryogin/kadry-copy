$(function () {
    getStickers(null, false);

    $('#sticker-form').on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serialize(),
            success: function (response) {
                if (response.action == 'reload') {
                    getStickers(window.location.href + '/stickers', false);
                } else if (response.action == 'alert') {
                    alert(response.message);
                }
            },
            error: function (response) {
            }
        });
        $('#add-sticker-modal').modal('hide');
    });
    $('#toggle-stickers').on('click', function () {
        if ($(this).text().trim() == "Закрытые стикеры") {
            getStickers(null, true);
        } else {
            getStickers(null, false);
        }
        $(this).toggleClass('active-stickers').toggleClass('closed-stickers').text(
            $(this).text().trim() == "Закрытые стикеры" ? "Открытые стикеры" : "Закрытые стикеры");

    });

    $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').data('target', $(e.relatedTarget).data('sticker_id'));
        $('#sticker-content').text($('#sticker-' + $(e.relatedTarget).data('sticker_id')).find('p.sticker-content').text());
    });

    $('#confirm-delete .btn-ok').on('click', function () {
        stickerRemove($(this).data('target'));
        $('#confirm-delete').modal('hide');
    });

    $('#add-sticker-modal').on('show.bs.modal', function (e) {
        $('#charcounter').text(50 - $('#sticker_text').val().length);
    });

    $('#sticker_text').on('keyup', function () {
        $('#charcounter').text(50 - $(this).val().length)
    }).on('keypress', function (e) {
        if (50 - $(this).val().length <= 0) {
            e.preventDefault();
        }
    });
});

function getStickers(dataString, deleted) {
    var url = deleted ? window.location.href + '/stickers/true' : window.location.href + '/stickers';
    $('#stickers-container .sticker').remove();
    $('#stickers-container').append($('<img>').addClass('loading').attr('src', '../icon/loading.gif'));
    $.ajax({
        type: "GET",
        url: url,
        data: dataString,
        success: function (response) {

            $('#stickers-container .sticker').remove();
            $('#stickers-container .loading').remove();
            $('#stickers-pagination').html(response.links);

            $('#stickers-pagination ul.pagination a').on('click', function (e) {
                e.preventDefault();
                var url = window.location.href + '/stickers';
                var dataString = 'page=' + $(this).attr('href').split('page=')[1];
                getStickers(dataString, response.deleted);
            });

            for (var i in response.data) {
                (function (i) {
                    var sticker = $('<div>')
                        .addClass('pull-left sticker add-shadows')
                        .css('background-color', '#' + response.data[i].color)
                        .attr('id', 'sticker-' + response.data[i].sticker_id)
                        .append($('<p>')
                            .append($('<span>').addClass('text-left').text(response.data[i].date)));

                    if (!response.deleted) {
                        sticker.find('p')
                            .append($('<button>')
                                .attr('data-target', '#confirm-delete').attr('data-toggle', 'modal')
                                .data('sticker_id', response.data[i].sticker_id)
                                .addClass('pull-right sticker-control btn btn-xs sticker-remove')
                                .append($('<i>').addClass('glyphicon glyphicon-remove-circle')))
                            .append($('<button>').addClass('pull-right sticker-control btn btn-xs sticker-edit')
                                .on('click', function () {
                                    stickerEdit(response.data[i].sticker_id)
                                }).append($('<i>').addClass('glyphicon glyphicon-pencil')))

                    } else {
                        sticker.find('p')
                            .append($('<button>').addClass('pull-right sticker-control btn btn-xs sticker-restore')
                                .on('click', function () {
                                    stickerRestore(response.data[i].sticker_id)
                                })
                                .append($('<i>').addClass('glyphicon glyphicon-open')))
                    }

                    sticker.append($('<p>').addClass('sticker-content').text(response.data[i].text))
                        .append($('<p>').addClass('text-center')
                            .text(response.data[i].author ? '(' + response.data[i].author + ')' : ''))
                        .append($('<div>').addClass('fold'))
                        .append($('<div>').addClass('fold2'))
                        .append($('<div>').addClass('ribbon'));
                    $('#stickers-container').append(sticker);
                }(i));
            }
        }
    });
}

function stickerEdit(id) {
    var form = $('#sticker-form');
    form.attr('action', window.location.href + '/sticker/' + id);
    form.attr('method', 'put');
    $("textarea[name='sticker_text']").val($('#sticker-' + id).find('p.sticker-content').text());
    $('#add-sticker-modal').modal('show');
    //getStickers(null, false);
}

function stickerRemove(id) {
    // confirmation request
    $.ajax({
        url: window.location.href + '/sticker/' + id,
        type: "DELETE",
        data: {id: id, _token: $('meta[name="csrf-token"]').attr('content')},
        success: function (response) {
            getStickers(null, false);
        }
    });
}

function stickerRestore(id) {
    $.ajax({
        url: window.location.href + '/sticker/' + id,
        type: "POST",
        data: {id: id, _token: $('meta[name="csrf-token"]').attr('content')},
        success: function (response) {
            getStickers(null, true);
        }
    });
}
//# sourceMappingURL=stickers_ajax.js.map
