$(function () {

    // display change-status modal
    $('button.status-label').on('click', function () {
        statusEvent($(this));
    });

    // display interview appointment
    $('select#status').on('change', function () {
        if ($(this).val() == 3) {
            $('div#status-select div.inteview-checker').slideDown();
        } else {
            $('div#status-select #interview').prop("checked", false);
            $('div#status-select div.inteview-checker').slideUp();
            $(this).parents('div.modal-body').find('div.interview-row').slideUp();
        }
    });

    // display event fields
    $('.appoint-checkbox').on('change', function () {
        $(this).parents('div.modal-body').find('div.interview-row').slideToggle();
    });

    // modal form ajax submit
    $('.modal-form').on('submit', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).data('method'),
            data: $(this).serialize(),
            success: function (response) {
                sendAjax(window.location.href + '/history', null, $('meta[name="page"]').attr('content'));
                if (response.action == 'added') {
                    addCandidateRow(response);
                } else if (response.action == 'updated') {
                    moveCandidate(response)
                }
            },
            error: function (response) {
                console.log(response.responseText);
                var errors = $.parseJSON(response.responseText);
                $.each(errors, function (key) {
                    $('[name="' + key + '"]').parents('div.form-group').addClass('has-error');
                });
            }
        });
    });

    // clear errors fields
    $('#status-select, #vacancy-select, #candidate-select').on('hidden.bs.modal', function () {
        $('div.form-group').removeClass('has-error');
        $('div#status-select div.inteview-checker').slideUp();
        $(this).find('div.interview-row').slideUp();
        $(this).find('#interview').prop("checked", false);
    });
});

// add candidate on ajax succees
function addCandidateRow(response) {
    console.log(response);
    var tr = $('<tr>').attr('id', "candidate-" + response.candidate_id)
        .append(
            $('<td>').append($('<a>').addClass('item')
                .attr('href', "/user/" + response.user_id + "/")
                .text(response.candidate_name))
        ).append(
            $('<td class="text-center">').text(response.candidate_salary)
        ).append(
            $('<td class="text-center">').append($('<span class="label label-info">')
                .text(response.updated))
        ).append(
            $('<td class="text-center">').append(
                $('<button>')
                    .addClass('btn btn-xs status-label ' + response.candidate_class)
                    .text(response.candidate_status)
                    .on('click', function () {
                        statusEvent($(this));
                    }).data('status', response.status_id))
        );

    $('div#status-' + response.status_id + ' table.candidates-table tbody').append(tr);
    $("div#status-" + response.status_id + " div.empty").css('display', 'none');
    $('select#user_id option[value="' + response.user_id + '"]').remove();
    $('#candidate-select').modal('hide');
}

// moves candidate on ajax succees
function moveCandidate(response) {
    ($('tr#candidate-' + response.id).find('button.status-label'))
        .removeClass(response.old_class)
        .addClass(response.new_class)
        .text(response.candidate_status)
        .data('status', response.status);

    $('tr#candidate-' + response.id).detach().appendTo($("div#status-" + response.status + " tbody"));
    $("div#status-" + response.status + " div.empty").css('display', 'none');
    // change status
    $('#status-select').modal('hide');
}

// adds event listener on status button
function statusEvent(button) {
    $('select#status').val(button.data('status'));
    $('#status-select').find('form').attr('action', '/candidate/' + button.parents('tr').attr('id').split('candidate-')[1]);
    $('#status-select span.modal-item').text(button.parents('tr').find('td:first').text().trim() + '?');
    $('#status-select').modal('show');
}
//# sourceMappingURL=vacancy_candidate_ajax.js.map
