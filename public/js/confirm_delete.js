$(function () {
    $('.delete-form').on('submit', function (e) {
        if (!confirm("Подтвердить удаление " +
                $(this).parents('tr:first').find('a.item').text().trim() +
                "?"
            )) {
            e.preventDefault();
        }
    });
});
//# sourceMappingURL=confirm_delete.js.map
