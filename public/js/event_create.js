$(function () {
    initializeDatePicker('#start-1');
    initializeDatePicker('#start-2');
    initializeDatePicker('#end-1');
    initializeDatePicker('#end-2');

    $('#event-color-1').colorpicker();
    $('#event-color-2').colorpicker();

    $('#title').val($('#type').find('option:selected').text());

    // adds title when type not interview
    $('#type').on('change', function () {
        if ($(this).val() == 1) {
            $('#title').val('');
            $('#title-div').slideDown();
        } else {
            $('#title').val($(this).find('option:selected').text());
            $('#title-div').slideUp();
        }
    });
});

function initializeDatePicker(id) {
    var date = $(id + ' input').val() ? moment($(id + ' input').val(), 'YYYY-MM-DD HH:mm:ss') : moment();
    if ($.trim($(id).html())) {
        $(id).datetimepicker({locale: 'ru'}).data("DateTimePicker").date(date);
    }
}

//# sourceMappingURL=event_create.js.map
