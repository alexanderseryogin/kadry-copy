$(function () {

    if (localStorage.selected) {
        $('#category').val(localStorage.selected);
    }

    $('#category').on('change', function () {
        var form = $(this).parents("form:first");
        localStorage.setItem('selected', $(this).val());
        var dataString = form.serialize();
        var formAction = form.attr('action');
        sendAjax(formAction, dataString);
    });
    $('#category').trigger('change');


});


function sendAjax(url, dataString) {
    $('#items-list, .pagination-container').html('');
    $('#loading-container').show();
    $.ajax({
        type: "GET",
        url: url,
        data: dataString,
        success: function (data) {
            $('tbody#items-list').html('');
            $('div.pagination-container').html(data.links);
            $('ul.pagination a').on('click', function (e) {
                paginate(e, $(this).attr('href').split('page=')[1]);
            });
            drawTable(data.data, data.edit, data.delete);
            $('#loading-container').hide();
        }
    });
}

function paginate(e, page) {
    e.preventDefault();
    var form = $('#category').parents("form:first");
    var dataString = form.serialize();
    dataString += '&page=' + page;
    var formAction = form.attr('action');
    sendAjax(formAction, dataString);
}

function drawTable(items, edit, del) {
    $(items).each(function () {
        var tr = $('<tr></tr>');
        tr.append($('<td>' + $(this)[0].id + '</td>').addClass('id-cell'));
        tr.append($('<td>').append($('<a href="dictionary/' + $(this)[0].id + '" class="item">' + $(this)[0].value + '</a>')));

        if (edit) {
            var edit_path = '/dictionary/' + $(this)[0].id + '/edit/';
            var edit_link = $('<a href="' + edit_path + '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>');
            tr.append($('<td>').append(edit_link));
        }

        if (del) {
            var delete_form = $('<form method="POST" action="/dictionary/' + $(this)[0].id + '" accept-charset="UTF-8" class="delete-form"></form>');
            delete_form.append('<input name="_method" type="hidden" value="DELETE">');
            delete_form.append('<input name="_token" type="hidden" value="' + $('meta[name="csrf-token"]').attr('content') + '">');
            delete_form.append('<button type="submit" class="btn btn-xs user-control"><i class="fa fa-btn fa-trash"></i></button>');
            delete_form.on('submit', function (e) {
                if (!confirm("Подтвердить удаление " +
                        $(this).parents('tr:first').find('a.item').text().trim() +
                        "?"
                    )) {
                    e.preventDefault();
                }
            });
            tr.append($('<td>').append(delete_form));
        }


        $('tbody#items-list').append(tr);
    });
}
$(function () {
    if (localStorage.selected) {
        $('#type').val(localStorage.selected);
    }

    $('#type').on('change', function () {
        localStorage.setItem('selected', $(this).val());
    });
});
//# sourceMappingURL=dictionary.js.map
