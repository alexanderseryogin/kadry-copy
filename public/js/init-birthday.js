$(function () {
    var id = '#b-day';
    var date = $(id + ' input').val() ? moment($(id + ' input').val(), 'DD-MM-YYYY') : moment();
    $(id).datetimepicker({locale: 'ru',format: 'DD.MM.YYYY'}).data("DateTimePicker").date(date);
});

//# sourceMappingURL=init-birthday.js.map
