<?php

namespace App\Listeners;

use App\User;
use App\Sticker;
use App\Candidate;
use App\Participant;
use App\Event;
use Illuminate\Http\UploadedFile;

class UserObserver
{
    protected $cv_file;
    protected $img_file;

    public function creating(User $user)
    {

    }

    public function created(User $user)
    {

    }

    public function updating(User $user)
    {

    }

    public function updated(User $user)
    {

    }

    public function saving(User $user)
    {
        $user->password = bcrypt('123456');
        $user->relocatable = $user->relocatable != null ? true : false;
        $user->proceedFileName();
    }

    public function saved(User $user)
    {
        $user->saveFiles();
    }

    public function deleting(User $user)
    {
        $unique = ['email' => null, 'skype' => null, 'phone' => null];
        $user->update($unique);
    }

    public function deleted(User $user)
    {
        $user->deleteFiles();

        // delete stickers where user is adressee
        Sticker::where('addressee_id', $user->id)->get()
            ->map(function ($sticker) {
                $sticker->forceDelete();
            });

        // delete candidats for this user
        Candidate::where('user_id', $user->id)->get()
            ->map(function ($candidate) {
                $candidate->delete();
            });


        // delete all events
        // where user is manager and no participants
        // or where user is the only participant

        Event::where(function ($query) use ($user) {
            $query->where('manager_id', $user->id);
            $query->whereDoesntHave('participants');
        })
            ->orWhere(function ($query) use ($user) {
                $query->whereHas('participants', function ($query) use ($user) {
                    $query->where('participants.user_id', $user->id);
                    $query->havingRaw('count(participants.user_id)=1');

                });
            })
            ->get()->map(function ($event) {
                $event->delete();
            });
        //            ->toSql();


        // delete participants where user_id
        Participant::where('user_id', $user->id)->get()
            ->map(function ($participant) {
                $participant->delete();
            });
    }

    public function restoring(User $user)
    {

    }

    public function restored(User $user)
    {

    }
}
