<?php

namespace App\Listeners;

use App\Vacancy;
use App\Candidate;
use App\Http\Requests\Request;
use App\Participant;

class VacancyObserver
{
    public function creating(Vacancy $vacancy)
    {

    }

    public function created(Vacancy $vacancy)
    {

    }

    public function updating(Vacancy $vacancy)
    {

    }

    public function updated(Vacancy $vacancy)
    {

    }

    public function saving(Vacancy $vacancy)
    {

    }

    public function saved(Vacancy $vacancy)
    {

    }

    public function deleting(Vacancy $vacancy)
    {

    }

    public function deleted(Vacancy $vacancy)
    {
        // delete candidats for this user
        Candidate::where('vacancy_id', $vacancy->id)->get()
            ->map(function ($candidate) {
                $candidate->delete();
            });
    }

    public function restoring(Vacancy $vacancy)
    {

    }

    public function restored(Vacancy $vacancy)
    {

    }
}
