<?php

namespace App\Listeners;

use App\Event;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;


class EventObserver
{


    public function creating(Event $event)
    {
        $event->start = strtotime($event->start);
        $event->end = strtotime($event->end);
        $event->manager_id = Auth::user()->id;
    }

    public function created(Event $event)
    {

    }

    public function updating(Event $event)
    {

    }

    public function updated(Event $event)
    {

    }

    public function saving(Event $event)
    {

    }

    public function saved(Event $event)
    {

    }

    public function deleting(Event $event)
    {

    }

    public function deleted(Event $event)
    {
        $event->removeParticipants();
    }

    public function restoring(Event $event)
    {

    }

    public function restored(Event $event)
    {

    }
}
