<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\DripEmailer;
use App\Event;
use App\Participant;
use App\User;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send drip e-mails to a user';

    /**
     * The drip e-mail service.
     *
     * @var DripEmailer
     */
    protected $drip;


    /**
     * Create a new command instance.
     * @param DripEmailer $drip
     *
     */
    public function __construct(DripEmailer $drip)
    {
        parent::__construct();

        $this->drip = $drip;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $events = Event::whereBetween('start', [
            Carbon::now()->addMinutes(30),
            Carbon::now()->addHour()])
            ->orWhereBetween('created_at', [
                Carbon::now()->subMinutes(30),
                Carbon::now()])
            ->get();

//        $events = Event::whereBetween('start', [
//            Carbon::yesterday(),
//            Carbon::tomorrow()])
//            ->get();

        $events->each(function ($event) {
            $event->participants->each(function ($participant) use ($event) {
                $this->drip->send($participant, $event);
            });
        });
    }
}
