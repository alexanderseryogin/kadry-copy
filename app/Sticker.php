<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sticker extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'id';

    protected $fillable = [
        'sticker_text',
        'author_id',
        'addressee_id',
        'color',
    ];

    /**
     * @return mixed
     */
    public function author()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * @return mixed
     */
    public function addressee()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }
}
