<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use App\Event;

class Participant extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'event_id',
        'user_id',
    ];

    protected $touches = ['event', 'user'];

    /**
     * @param \App\Event $event
     * @param array $participants
     * @throws ValidationException
     */
    public static function storeParticipants(Event $event, array $participants)
    {
        $validator = Validator::make($participants, [
            '*' => 'integer|exists:users,id',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator, new JsonResponse(['participants[]' => ['Wrong participants id\'s']], 422));
        }


        // Удалить те, которые не пришли в массиве.
        self::where('event_id', $event->id)->whereNotIn('user_id', $participants)->get()
            ->map(function ($participant) {
                $participant->delete();
            });

        // Оставляю только тех, которых нет в базе
        $participants=collect($participants)
            ->diff(self::where('event_id', $event->id)->whereIn('user_id', $participants)->pluck('user_id'));

            foreach ($participants as $participant_id) {
                $participant = new self;
                $participant->user_id = $participant_id;
                $participant->event_id = $event->id;
                $participant->save();
            }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}