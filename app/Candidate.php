<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Vacancy;
use App\Dictionary;

class Candidate extends Model
{
    const STATUS_FOUNDED = 1;
    const STATUS_SELECTED = 2;
    const STATUS_INTERVIEW = 3;
    const STATUS_CONFIRMED = 4;
    const STATUS_UNSUITABLE = 5;
    const STATUS_REFUSED = 6;

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'vacancy_id',
        'status',
    ];

    /**
     * @var array
     */
    public static $stringFields = [
        'salary',
        'currency',
        'title',
    ];

    /**
     * @return array|null
     */
    public static function getStatus()
    {
        static $list = null;
        if (empty($list)) {
            $list = [
                self::STATUS_FOUNDED => 'Найденные',
                self::STATUS_SELECTED => 'Отобранные',
                self::STATUS_INTERVIEW => 'Собеседование',
                self::STATUS_CONFIRMED => 'Утвержден',
                self::STATUS_UNSUITABLE => 'Не подходит',
                self::STATUS_REFUSED => 'Отказался',
            ];
        }
        return $list;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vacancy()
    {
        return $this->belongsTo(Vacancy::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param $status
     * @return mixed
     */
    public static function getCssClass($status)
    {
        static $list = null;
        if (empty($list)) {
            $list = [
                self::STATUS_FOUNDED => 'type-founded',
                self::STATUS_SELECTED => 'type-selected',
                self::STATUS_INTERVIEW => 'type-interview',
                self::STATUS_CONFIRMED => 'type-confirmed',
                self::STATUS_UNSUITABLE => 'type-unsuitable',
                self::STATUS_REFUSED => 'type-refused',
            ];
        }
        return $list[$status];
    }

    /**
     * @return array
     */
    public function getRules()
    {
        return [
            "user_id" => "required|integer|exists:users,id",
            "vacancy_id" => "required|integer|exists:vacancies,id|unique:candidates,vacancy_id,NULL,vacancy_id,user_id,$this->user_id",
        ];
    }
}