<?php
namespace App\Parsers;

use App\Parsers\UserCreator;
use Illuminate\Http\UploadedFile;
use App\User;


class DocxParser
{
    use UserCreator;
    protected $user;

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function parse(UploadedFile $file)
    {
        $text = '';
        $zip = zip_open($file->path());
        if (!$zip || is_numeric($zip)) return false;
        while ($zip_entry = zip_read($zip)) {
            if (zip_entry_open($zip, $zip_entry) == FALSE) continue;
            if (zip_entry_name($zip_entry) != "word/document.xml") continue;
            $text .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
            zip_entry_close($zip_entry);
        }

        zip_close($zip);

        $text = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $text);
        $text = str_replace('</w:r></w:p>', "\r\n", $text);
        $text = strip_tags($text);
        return [$this->parseText($text, $file), $this->user];
    }
}