<?php
namespace App\Parsers;

use Illuminate\Http\UploadedFile;
use \PhpOffice\PhpWord\IOFactory;
use App\User;


class DocParser
{
    use UserCreator;
    /**
     * The parser implementation.
     */
    protected $parser;
    protected $user;

    /**
     * WordParser constructor.
     */
    public function __construct()
    {
        $this->parser = IOFactory::createReader('MsDoc');
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function parse(UploadedFile $file)
    {
        $phpWord = $this->parser->load($file->path());
        $text = '';
        foreach ($phpWord->getSections() as $section) {
            $arrays = $section->getElements();
            foreach ($arrays as $e) {
                if (get_class($e) === 'PhpOffice\PhpWord\Element\TextRun') {
                    foreach ($e->getElements() as $text) {
                        $text .= $text->getText();
                    }
                } else if (get_class($e) === 'PhpOffice\PhpWord\Element\TextBreak') {
                    $text .= "\r\n";
                } else if (get_class($e) === 'PhpOffice\PhpWord\Element\Table') {
                    $rows = $e->getRows();
                    foreach ($rows as $row) {
                        $cells = $row->getCells();
                        foreach ($cells as $cell) {
                            $celements = $cell->getElements();
                            foreach ($celements as $celem) {
                                if (get_class($celem) === 'PhpOffice\PhpWord\Element\Text') {
                                    $text .= $celem->getText();
                                } else if (get_class($celem) === 'PhpOffice\PhpWord\Element\TextRun') {
                                    foreach ($celem->getElements() as $text) {
                                        $text .= $text->getText();
                                    }
                                }
                            }
                        }
                    }

                } else {
                    $text .= $e->getText();
                }
            }
            break;
        }

        return [$this->parseText($text, $file), $this->user];
    }
}