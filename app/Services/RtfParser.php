<?php
namespace App\Parsers;

use App\Parsers\UserCreator;
use Illuminate\Http\UploadedFile;
use \PhpOffice\PhpWord\IOFactory;
use App\User;

class RtfParser
{
    use UserCreator;
    /**
     * The parser implementation.
     */
    protected $parser;
    protected $user;


    /**
     * WordParser constructor.
     */
    public function __construct()
    {
        $this->parser = IOFactory::createReader('RTF');
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function parse(UploadedFile $file)
    {
        $phpWord = $this->parser->load($file->path());
        //throw new \HttpInvalidParamException();
        $body = 'Не удалось прочитать файл. Формат Rtf пока не поддерживается.';
//        foreach ($phpWord->getSections() as $section) {
//            $arrays = $section->getElements();
//            foreach ($arrays as $e) {
//                if (get_class($e) === 'PhpOffice\PhpWord\Element\TextRun') {
//                    foreach ($e->getElements() as $text) {
//                        $body .= $text->getText();
//                    }
//                } else if (get_class($e) === 'PhpOffice\PhpWord\Element\TextBreak') {
//                    $body .= "\r\n";
//                } else if (get_class($e) === 'PhpOffice\PhpWord\Element\Table') {
//                    $rows = $e->getRows();
//                    foreach ($rows as $row) {
//                        $cells = $row->getCells();
//                        foreach ($cells as $cell) {
//                            $celements = $cell->getElements();
//                            foreach ($celements as $celem) {
//                                if (get_class($celem) === 'PhpOffice\PhpWord\Element\Text') {
//                                    $body .= $celem->getText();
//                                } else if (get_class($celem) === 'PhpOffice\PhpWord\Element\TextRun') {
//                                    foreach ($celem->getElements() as $text) {
//                                        $body .= $text->getText();
//                                    }
//                                }
//                            }
//                        }
//                    }
//
//                } else {
//                    $body .= $e->getText();
//                }
//            }
//            break;
//        }
        return $body;
    }
}