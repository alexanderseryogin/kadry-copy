<?php

namespace App\Parsers;

use App\User;
use Illuminate\Http\UploadedFile;
use App\Company;

trait UserCreator
{
    public function parseText($text, UploadedFile $file)
    {
        $this->user = new User;
        $this->user->cv_name = $file;
        $this->user->about = $text;
        $parsed = [];
        $patterns = [
            'fio' => '/(\p{Lu}[\p{Ll}]+[\-\s]?){2,3}/uSD',
            'phone' => '/[+]?[-0-9\(\) ]{7,18}/uSD',
            'date' => '/((\p{N}{1,2}|((jan|янв|feb|фев|mar|мар|apr|апр|may|май|мая|jun|июн|jul|июл|aug|авг|sep|сен|oct|окт|nov|ноя|dec|дек)\p{L}{0,6}))([\.\p{Z}\,\/]{1,2})){2}(\p{N}{2,4})/iuSD',
            'email' => '/\p{Ll}*[\.\-\_\p{N}\p{Ll}]*@\p{Ll}*\.\p{Ll}{2,3}(\.\p{Ll}{2,3})?/uSD',
            'skype' => '/skype:\s[^\p{Z}]*/iuSD',
            'urls' => '/http[^\p{Z}]*/uSD',
        ];

        foreach ($patterns as $key => $val) {
            preg_match_all($val, preg_replace('/\s+/', ' ', $text), $parsed[$key]);
        }

        // proceed parsed data
        foreach ($parsed as $key => $val) {
            if ($key !== 'urls') {
                if (isset($val[0]) && isset($val[0][0]) && method_exists($this, $key)) {
                    $this->$key($val[0][0]);
                }
            } else {
                $social_networks = [
                    'facebook' => '/https:\/\/www.facebook.com\/[^\p{Z}]*/u',
                    'vk' => '/https:\/\/vk.com\/[^\p{Z}]*/u',
                    'linkedin' => '/https:\/\/www.linkedin.com\/[^\p{Z}]*/u',
                    'google_plus' => '/https:\/\/plus.google.com\/[^\p{Z}]*/u',
                ];
                foreach ($social_networks as $network => $pattern) {
                    $this->get_social($val[0], $network, $pattern);
                }
                $this->user_links($val[0]);
            }
        }

        $this->company($text);
        return $text;
    }

    private function fio($fio)
    {
        $arr = explode(' ', trim($fio), 3);
        switch (count($arr)) {
            case 1:
                $this->user->first_name = $arr[0];
                break;
            case 2:
                $this->user->first_name = $arr[0];
                $this->user->last_name = $arr[1];
                break;
            case 3:
                $this->user->last_name = $arr[0];
                $this->user->first_name = $arr[1];
                $this->user->patronymic = $arr[2];
                break;
        }
    }

    private function phone($phone)
    {
        $this->user->phone = $phone;
    }

    private function date($date)
    {
        if (strtotime($date)) {
            $this->user->birthday = date('d.m.Y', strtotime($date));
        } else {
            $date = preg_replace('/[^\p{N}\p{L}]/iu', '.', $date);
            $date = preg_replace('/\.{2,}/iu', '.', $date);
            if (strtotime($date)) {
                $this->user->birthday = date('d.m.Y', strtotime($date));
            } else {
                $months = [
                    'January' => ['jan', 'янв'],
                    'February' => ['feb', 'фев'],
                    'March' => ['mar', 'мар'],
                    'April' => ['apr', 'апр'],
                    'May' => ['may', 'мая'],
                    'June' => ['jun', 'июн'],
                    'July' => ['jul', 'июл'],
                    'August' => ['aug', 'авг'],
                    'September' => ['sep', 'сен'],
                    'October' => ['oct', 'окт'],
                    'November' => ['nov', 'ноя'],
                    'December' => ['dec', 'дек'],
                ];
                foreach ($months as $month => $arr) {
                    $date = preg_replace('/(' . $arr[0] . '|' . $arr[1] . ')\p{L}*/iuSD', $month, $date);
                }

                if (strtotime($date)) {
                    $this->user->birthday = date('d.m.Y', strtotime($date));
                } else {

                    $date = preg_replace('/\./u', '/', $date);
                    if (strtotime($date)) {
                        $this->user->birthday = date('d.m.Y', strtotime($date));
                    }
                }
            }
        }
    }

    private function email($email)
    {
        $this->user->email = $email;
    }

    private function skype($skype)
    {
        $this->user->skype = preg_replace('/skype:\s/iu', '', $skype);
    }

    private function get_social(array &$urls, $network, $pattern)
    {
        foreach ($urls as $key => $url) {
            if (preg_match($pattern, $url)) {
                $this->user->$network = $url;
                unset($urls[$key]);
                break;
            }
        }
    }

    private function user_links(array $urls)
    {
        $this->user->user_links = '';
        $i = 1;
        $len = count($urls);
        foreach ($urls as $url) {
            $this->user->user_links .= $url;
            if ($i !== $len)
                $this->user->user_links .= ', ';
            $i++;
        }
    }

    private function company($text)
    {
        Company::all()->map(function ($company) use ($text) {
            if (preg_match('/' . $company->title . '/iuSD', $text))
                $this->user->company_id = $company->id;
        });

        return false;
    }

}




