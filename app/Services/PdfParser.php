<?php
namespace App\Parsers;

use App\Parsers\UserCreator;
use Illuminate\Http\UploadedFile;
use \Smalot\PdfParser\Parser;
use App\User;


class PdfParser
{
    use UserCreator;
    /**
     * @var Parser
     */
    protected $parser;
    protected $user;


    /**
     * PdfParser constructor.
     * @param Parser $parser
     */
    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function parse(UploadedFile $file)
    {
        $pdf = $this->parser->parseFile($file->path());
        return [$this->parseText($pdf->getText(), $file), $this->user];
    }
}