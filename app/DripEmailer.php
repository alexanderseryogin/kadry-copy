<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class DripEmailer extends Model
{
    public function send(Participant $participant, Event $event)
    {
        try {
            Mail::send('emails.reminder',
                [
                    'user' => $participant->user,
                    'event' => $event,
                    'participants'=>$event->participants(),
                    'date' => Carbon::now()->format('d.m H:i')],
                function ($m) use ($participant) {
                    $m->from('hrm@faceit_hrm', 'FaceIT HRM');
                    $m->to($participant->user->email)->subject('Notification');
                });
            echo 'notification send succsesfully to ' .
                $participant->user->first_name .
                ' ' . $participant->user->last_name .
                PHP_EOL;
        } catch (\Exception $exception) {
            echo
                'error occurred while delivering to ' .
                $participant->user->first_name .
                ' ' . $participant->user->last_name .
                PHP_EOL;
        }
    }
}
