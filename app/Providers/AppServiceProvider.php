<?php

namespace App\Providers;

use App\Event;
use App\Vacancy;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Illuminate\Support\ServiceProvider;
use App\Listeners\UserObserver;
use App\Listeners\EventObserver;
use App\Listeners\VacancyObserver;
use App\User;
use Validator;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Event::observe(EventObserver::class);
        Vacancy::observe(VacancyObserver::class);


        Validator::extend('freetime', function ($attribute, $value, $parameters, $validator) {
            if (count($parameters) !== 2) {
                throw new InvalidArgumentException('freetime needs two parameters');
            }
            list($start, $end) = $parameters;

            if ($start > $end) {
                throw new InvalidArgumentException('start time should be less then end time');
            }

            if (!Carbon::parse($start) || !Carbon::parse($end)) {
                throw new InvalidArgumentException('arguments should be date string \'Y-m-d H:i:s\'');
            }
            $free = Event::where($attribute, $value)
                ->where('start', '<', $end)
                ->where('end', '>', $start)->count();
            return !$free;
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
