<?php

namespace App\Providers;

use App\Candidate;
use App\Dictionary;
use App\Participant;
use App\Vacancy;
use App\User;
use App\Event;
use App\Sticker;
use App\Company;
use App\Policies\UserPolicy;
use App\Policies\CandidatePolicy;
use App\Policies\DictionaryPolicy;
use App\Policies\EventPolicy;
use App\Policies\StickerPolicy;
use App\Policies\VacancyPolicy;
use App\Policies\CompanyPolicy;
use App\Policies\ParticipantPolicy;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        User::class => UserPolicy::class,
        Candidate::class => CandidatePolicy::class,
        Dictionary::class => DictionaryPolicy::class,
        Event::class => EventPolicy::class,
        Sticker::class => StickerPolicy::class,
        Vacancy::class => VacancyPolicy::class,
        Company::class => CompanyPolicy::class,
        Participant::class => ParticipantPolicy::class,
    ];


    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //
    }
}
