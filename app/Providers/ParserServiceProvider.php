<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Parsers\PdfParser;
use App\Parsers\DocParser;
use App\Parsers\DocxParser;
use App\Parsers\RtfParser;

class ParserServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('pdf', PdfParser::class);
        $this->app->bind('doc', DocParser::class);
        $this->app->bind('docx', DocxParser::class);
        $this->app->bind('zip', DocxParser::class);
        $this->app->bind('rtf', RtfParser::class);
    }

    public function provides()
    {
        return ['pdf', 'doc', 'docx', 'zip', 'rtf'];
    }
}
