<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Dictionary;

class DictionaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Dictionary::getDictionaryTypes(true);
        return view('dictionary.index', ['categories' => $categories, 'page' => 'dictionary']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = new Dictionary;
        if (Auth::user()->cannot('create', $item)) {
            abort(403);
        }

        return view('dictionary.create', [
            'item' => $item,
            'types' => Dictionary::getDictionaryTypes(),
            'action' => 'DictionaryController@store',
            'page' => 'dictionary-edit',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->cannot('create', new Dictionary)) {
            abort(403);
        }

        $this->validate($request, Dictionary::getRules());
        Dictionary::create($request->all());
        return redirect('dictionary');
    }

    /**
     * Display the specified resource.
     *
     * @param  Dictionary $item
     * @return \Illuminate\Http\Response
     */
    public function show(Dictionary $item)
    {
        if (Auth::user()->cannot('read', $item)) {
            abort(403);
        }
        return view('dictionary.single', ['item' => $item, 'page' => 'dictionary']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Dictionary $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Dictionary $item)
    {
        if (Auth::user()->cannot('update', $item)) {
            abort(403);
        }
        return view('dictionary.edit', [
            'item' => $item,
            'page' => 'dictionary-edit'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Dictionary $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dictionary $item)
    {
        if (Auth::user()->cannot('update', $item)) {
            abort(403);
        }

        $this->validate($request, $item->getRules());
        $item->update($request->all());

        return redirect('dictionary');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Dictionary $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dictionary $item)
    {
        if (Auth::user()->cannot('delete', $item)) {
            abort(403);
        }

        try {
            $item->delete();
        } catch (QueryException $e) {
            abort(500, $item->value . ' используется некоторыми моделями приложения. Удаление невозможно!');
        }
        return redirect('dictionary');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggle(Request $request)
    {
        if ($request->isMethod('get')) {
            $request->flashOnly($request->category);
            $result = Dictionary::where('type', '=', $request->category)->paginate(20);
            $links = empty($result->links()) ? '' : $result->links()->toHtml();
            $response = [
                'data' => $result->getCollection(),
                'links' => $links,
                'edit' => Auth::user()->can('update', new Dictionary),
                'delete' => Auth::user()->can('delete', new Dictionary),
            ];
            return response()->json($response);
        }
    }


}