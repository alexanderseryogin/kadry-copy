<?php

namespace App\Http\Controllers;

use App\Event;
use App\User;
use App\Participant;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Jobs\SendReminderEmail;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Carbon\Carbon;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $events = Event::where(function ($query) use ($id) {
            $query->where('events.manager_id', $id);
            $query->orWhereHas('participants', function ($query) use ($id) {
                $query->where('user_id', $id);
            });
        })
            ->get();
        $calendar = \Calendar::addEvents($events)->setOptions(['eventLimit' => true, 'firstDay' => 1]);
        return view('calendar.index', [
            'calendar' => $calendar,
            'page' => 'events',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $event = new Event;
        if (Auth::user()->cannot('create', $event)) {
            abort(403);
        }

        return view('calendar.create', $this->getFormArgs('EventController@store', $event, $request->user()->id));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector|null
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $event = new Event;
        if (Auth::user()->cannot('create', $event)) {
            abort(403);
        }

        DB::beginTransaction();
        try {
            $this->validate($request, Event::getRules($request->start, $request->end));
            $event = Event::create([
                'start' => strtotime($request->start),
                'end' => strtotime($request->end),
                'manager_id' => $request->user()->id,
                'title' => $request->title,
                'color' => $request->color,
                'description' => $request->description,
            ]);
            Participant::storeParticipants($event, $request->participants ? $request->participants : []);
        } catch (ValidationException $e) {
            DB::rollback();
            return $e->response;
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::Commit();

        return redirect('events');
    }

    /**
     * Display the specified resource.
     *
     * @param  Event $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        if (Auth::user()->cannot('read', $event)) {
            abort(403);
        }

        return view('calendar.single', [
            'event' => $event,
            'page' => 'event'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Event $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Event $event)
    {
        if (Auth::user()->cannot('update', $event)) {
            abort(403);
        }

        return view('calendar.create', $this->getFormArgs('EventController@update', $event, $request->user()->id));
    }

    /**
     * @param Request $request
     * @param Event $event
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function update(Request $request, Event $event)
    {
        if (Auth::user()->cannot('update', $event)) {
            abort(403);
        }


        DB::beginTransaction();
        try {
            $this->validate($request, Event::getRules($request->start, $request->end));
            $event->update([
                'start' => strtotime($request->start),
                'end' => strtotime($request->end),
                'manager_id' => $request->user()->id,
                'title' => $request->title,
                'color' => $request->color,
                'description' => $request->description,
            ]);
            Participant::storeParticipants($event, $request->participants ? $request->participants : []);

        } catch (ValidationException $e) {
            DB::rollback();
            return $e->response;
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::Commit();

        return redirect('events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Event $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        if (Auth::user()->cannot('delete', $event)) {
            abort(403);
        }
        $event->delete();
        return redirect('events');
    }

    /**
     * @param $action
     * @param Event $event
     * @param $manager_id
     * @return array
     */
    protected function getFormArgs($action, Event $event, $manager_id)
    {
        $participants = User::select(DB::raw('CONCAT(last_name, " ", first_name) AS full_name'), 'id')->where('id', '<>', $manager_id)->pluck('full_name', 'id');
        return [
            'event' => $event,
            'action' => $action,
            'method' => $event->id ? 'put' : 'post',
            'participants' => $participants,
            'types' => Event::getTypes(),
            'selected' => $event->participants->pluck('user_id')->toArray(),
            'page' => 'event-edit'
        ];
    }
}