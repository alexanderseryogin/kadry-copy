<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Access\Gate;
use App\Candidate;
use App\History;
use App\User;
use App\Company;
use App\Dictionary;
use App\Vacancy;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Event;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('user.index', ['users' => $users, 'page' => 'users']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->cannot('create', new User)) {
            abort(403);
        }

        return view('auth.register', $this->getFormArgs('UserController@store', new User));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->cannot('create', new User)) {
            abort(403);
        }

        $this->validate($request, User::getRules());

        User::create($request->all());
        return redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {
        if (Auth::user()->cannot('read', $user)) {
            abort(403);
        }

        $vacancies = Vacancy::whereNotIn('id', $user->candidate->pluck('vacancy_id'))->orderBy('title')->pluck('title', 'id');
        $participants = User::select(DB::raw('CONCAT(last_name, " ", first_name) AS full_name'), 'id')->where('id', '<>', $request->user()->id)->pluck('full_name', 'id');
        return view('user.single', [
            'user' => $user,
            'vacancies' => $vacancies,
            'participants' => $participants,
            'statuses' => Candidate::getStatus(),
            'candidate' => $user->candidate,
            'page' => 'user',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if (Auth::user()->cannot('update', $user)) {
            abort(403);
        }

        return view('auth.register', $this->getFormArgs('UserController@update', $user));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if (Auth::user()->cannot('update', $user)) {
            abort(403);
        }

        $this->validate($request, User::getRules($user->id));
        $user->update($request->all());
        return redirect('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        if (Auth::user()->cannot('delete', $user)) {
            abort(403);
        }

        DB::beginTransaction();
        try {
            $user->delete();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::Commit();
        return redirect('user');
    }

    /**
     * @param $action
     * @param User $user
     * @param string $parsed
     * @return array
     */
    protected function getFormArgs($action, User $user, $parsed = '')
    {
        $method = $user->id ? 'put' : 'post';
        $positions = Dictionary::getDictionary(Dictionary::TYPE_POSITION, true);
        $currencies = Dictionary::getDictionary(Dictionary::TYPE_CURRENCY, true);
        $residences = Dictionary::getDictionary(Dictionary::TYPE_RESIDENCE, true);
        $companies = Company::getCompanies(true);
        $locations = Dictionary::getDictionary(Dictionary::TYPE_LOCATION, true);
        return [
            'user' => $user,
            'parsed' => $parsed,
            'action' => $action,
            'method' => $method,
            'positions' => $positions,
            'currencies' => $currencies,
            'residences' => $residences,
            'companies' => $companies,
            'locations' => $locations,
            'page' => 'user-edit',
        ];
    }

    /**
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download(User $user)
    {
        $file = 'uploads/cvs/' . $user->id;
        return response()->download($file, $user->cv_name);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setVacancy(Request $request, User $user)
    {
        $candidate = new Candidate;
        $candidate->user_id = $user->id;
        $candidate->vacancy_id = $request->vacancy_id;
        $candidate->save();
        return redirect()->action('UserController@show', ['id' => $user->id]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function history(Request $request, User $user)
    {
        if ($request->isMethod('get')) {
            $result = $user->history()->paginate(5);
            $links = empty($result->links()) ? '' : $result->links()->toHtml();

            $response = ['data' => [],
                'links' => $links,
            ];

            $i = 0;
            foreach ($result->getCollection() as $history) {
                $response['data'][$i]['manager'] = $history->manager->last_name . ' ' . $history->manager->first_name;
                $response['data'][$i]['user'] = $history->user->last_name . ' ' . $history->user->first_name;
                $response['data'][$i]['vacancy'] = $history->vacancy->title;
                $response['data'][$i]['old_val'] = $history->old_val ? Candidate::getStatus()[$history->old_val] : '';
                $response['data'][$i]['old_class'] = $history->old_val ? Candidate::getCssClass($history->old_val) : '';
                $response['data'][$i]['new_val'] = $history->new_val ? Candidate::getStatus()[$history->new_val] : '';
                $response['data'][$i]['new_class'] = $history->new_val ? Candidate::getCssClass($history->new_val) : '';
                $response['data'][$i]['action'] = History::getActionType()[$history->action_type];
                $response['data'][$i]['date'] = $history->created_at->format('d.m.y в H:i:s');
                ++$i;
            }

            return response()->json($response);
        }
    }

    public function upload(Request $request)
    {
        $array = null;
        if ($request->isMethod('post')) {
            $array = User::fromFile($request->file('cv'));
        } else if ($request->isMethod('get')) {
            $array = [$request->old('about'), new User,];
        }
        return view('user.parsed', $this->getFormArgs('UserController@store', $array[1], $array[0]));
    }


}
