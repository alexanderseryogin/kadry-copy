<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vacancy;
use App\Dictionary;
use App\Http\Requests;
use App\Candidate;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Event;
use App\History;

class VacancyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vacancies = Vacancy::all();
        return view('vacancy.index', ['vacancies' => $vacancies, 'page' => 'vacancies']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->cannot('create', new Vacancy)) {
            abort(403);
        }
        return view('vacancy.create', $this->getFormArgs('VacancyController@store', new Vacancy));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->cannot('create', new Vacancy)) {
            abort(403);
        }

        $this->validate($request, Vacancy::getRules());
        Vacancy::create($request->all());
        return redirect('vacancy');
    }

    /**
     * Display the specified resource.
     *
     * @param  Vacancy $vacancy
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Vacancy $vacancy)
    {
        if (Auth::user()->cannot('read', $vacancy)) {
            abort(403);
        }

        $candidates = Candidate::where('vacancy_id', $vacancy->id)->get();
        $selected = [];
        foreach ($candidates as $candidate) {
            $selected[$candidate->status]['candidates'][] = $candidate;
        }

        $users = User::select(DB::raw('CONCAT(last_name, " ", first_name) AS full_name'), 'id')->whereNotIn('id', Candidate::where('vacancy_id', $vacancy->id)->lists('user_id'))->pluck('full_name', 'id');
        $participants = User::select(DB::raw('CONCAT(last_name, " ", first_name) AS full_name'), 'id')->where('id', '<>', $request->user()->id)->pluck('full_name', 'id');
        return view('vacancy.single', [
            'vacancy' => $vacancy,
            'statuses' => Candidate::getStatus(),
            'selected' => $selected,
            'users' => $users,
            'types' => Event::getTypes(),
            'participants' => $participants,
            'page' => 'vacancy',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  Vacancy $vacancy
     * @return \Illuminate\Http\Response
     */
    public function edit(Vacancy $vacancy)
    {
        if (Auth::user()->cannot('update', $vacancy)) {
            abort(403);
        }
        return view('vacancy.create', $this->getFormArgs('VacancyController@update', $vacancy));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Vacancy $vacancy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vacancy $vacancy)
    {
        if (Auth::user()->cannot('update', $vacancy)) {
            abort(403);
        }

        $this->validate($request, Vacancy::getRules());
        $vacancy->update($request->all());
        return redirect('vacancy');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Vacancy $vacancy
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Vacancy $vacancy)
    {
        if (Auth::user()->cannot('delete', $vacancy)) {
            abort(403);
        }
        DB::beginTransaction();
        try {
            $vacancy->delete();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::Commit();

        return redirect('vacancy');
    }

   /**
     * @param $action
     * @param Vacancy $vacancy
     * @return array
     */
    protected function getFormArgs($action, Vacancy $vacancy)
    {
        $method = $vacancy->id ? 'put' : 'post';
        $currencies = Dictionary::getDictionary(Dictionary::TYPE_CURRENCY, true);
        $locations = Dictionary::getDictionary(Dictionary::TYPE_LOCATION, true);
        return [
            'vacancy' => $vacancy,
            'action' => $action,
            'method' => $method,
            'locations' => $locations,
            'currencies' => $currencies,
            'page' => 'vacancy-edit',
        ];
    }

    /**
     * @param Request $request
     * @param Vacancy $vacancy
     * @return \Illuminate\Http\JsonResponse
     */
    public function history(Request $request, Vacancy $vacancy)
    {
        if ($request->isMethod('get')) {
            $result = $vacancy->history()->paginate(5);
            $links = empty($result->links()) ? '' : $result->links()->toHtml();

            $response = ['data' => [],
                'links' => $links,
            ];

            $i = 0;
            foreach ($result->getCollection() as $history) {
                $response['data'][$i]['manager'] = $history->manager->last_name . ' ' . $history->manager->first_name;
                $response['data'][$i]['user'] = $history->user->last_name . ' ' . $history->user->first_name;
                $response['data'][$i]['vacancy'] = $history->vacancy->title;
                $response['data'][$i]['old_val'] = $history->old_val ? Candidate::getStatus()[$history->old_val] : '';
                $response['data'][$i]['old_class'] = $history->old_val ? Candidate::getCssClass($history->old_val) : '';
                $response['data'][$i]['new_val'] = $history->new_val ? Candidate::getStatus()[$history->new_val] : '';
                $response['data'][$i]['new_class'] = $history->new_val ? Candidate::getCssClass($history->new_val) : '';
                $response['data'][$i]['action'] = History::getActionType()[$history->action_type];
                $response['data'][$i]['date'] = $history->created_at->format('d.m.y в H:i:s');
                ++$i;
            }

            return response()->json($response);
        }
    }
}
