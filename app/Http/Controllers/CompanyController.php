<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Dictionary;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        return view('company.index', ['companies' => $companies, 'page' => 'companies']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = new Company;
        if (Auth::user()->cannot('create', $company)) {
            abort(403);
        }
        return view('company.create', $this->getFormArgs('CompanyController@store', $company));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->cannot('create', new Company)) {
            abort(403);
        }

        $this->validate($request, Company::getRules());
        Company::create($request->all());
        return redirect('company');
    }

    /**
     * Display the specified resource.
     *
     * @param  Company $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        if (Auth::user()->cannot('read', $company)) {
            abort(403);
        }

        return view('company.single', [
            'company' => $company,
            'page' => 'company'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Company $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        if (Auth::user()->cannot('update', $company)) {
            abort(403);
        }
        return view('company.create', $this->getFormArgs('CompanyController@update', $company));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Company $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        if (Auth::user()->cannot('update', $company)) {
            abort(403);
        }
        $this->validate($request, Company::getRules());
        $company->update($request->all());
        return redirect('company');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Company $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        if (Auth::user()->cannot('delete', $company)) {
            abort(403);
        }

        try {
            $company->delete();
        } catch (QueryException $e) {
            abort(500, 'Компания ' . $company->title . ' используется некоторыми моделями приложения. Удаление невозможно!');
        }
        return redirect('company');
    }

    /**
     * @param $action
     * @param Company $company
     * @return array
     */
    protected function getFormArgs($action, Company $company)
    {
        $method = $company->id ? 'put' : 'post';
        $locations = Dictionary::getDictionary(Dictionary::TYPE_LOCATION, true);
        return [
            'company' => $company,
            'action' => $action,
            'method' => $method,
            'locations' => $locations,
            'page' => 'company-edit'
        ];
    }
}
