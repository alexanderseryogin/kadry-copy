<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\History;
use App\Candidate;
use App\Event;
use Carbon\Carbon;
use App\Sticker;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class OrganizerController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = [];
        if (Auth::user()->can('assign', new Sticker)) {
            $users = User::select(DB::raw('CONCAT(last_name, " ", first_name) AS full_name'), 'id')->pluck('full_name', 'id');
        }
        return view('calendar.organizer', [
            'page' => 'organizer',
            'users' => $users,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function history(Request $request)
    {
        if ($request->isMethod('get')) {
            $result = History::orderBy('created_at', 'desc')->paginate(10);
            $links = empty($result->links()) ? '' : $result->links()->toHtml();
            $response = ['data' => [],
                'links' => $links,
            ];
            $i = 0;
            foreach ($result->getCollection() as $history) {
                $response['data'][$i]['manager'] = $history->manager->last_name . ' ' . $history->manager->first_name;
                $response['data'][$i]['user'] = $history->user->last_name . ' ' . $history->user->first_name;
                $response['data'][$i]['vacancy'] = $history->vacancy->title;
                $response['data'][$i]['old_val'] = $history->old_val ? Candidate::getStatus()[$history->old_val] : '';
                $response['data'][$i]['old_class'] = $history->old_val ? Candidate::getCssClass($history->old_val) : '';
                $response['data'][$i]['new_val'] = $history->new_val ? Candidate::getStatus()[$history->new_val] : '';
                $response['data'][$i]['new_class'] = $history->new_val ? Candidate::getCssClass($history->new_val) : '';
                $response['data'][$i]['action'] = History::getActionType()[$history->action_type];
                $response['data'][$i]['date'] = $history->created_at->format('d.m.y в H:i:s');
                ++$i;
            }
            return response()->json($response);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function events(Request $request)
    {
        if ($request->isMethod('get')) {
            $id = $request->user()->id;
            $result = Event::where('events.start', '>', Carbon::now())
                ->where(function ($query) use ($id) {
                    $query->where('events.manager_id', $id);
                    $query->orWhereHas('participants', function ($query) use ($id) {
                        $query->where('user_id', $id);
                    });
                })
                ->paginate(20);
            $links = empty($result->links()) ? '' : $result->links()->toHtml();
            $response = ['data' => [],
                'links' => $links,
            ];
            $i = 0;
            foreach ($result->getCollection() as $event) {
                $response['data'][$i]['title'] = $event->title;
                $response['data'][$i]['id'] = $event->id;
                $response['data'][$i]['color'] = $event->color;
                $response['data'][$i]['description'] = $event->description;
                $response['data'][$i]['date'] = $event->start->format('d.m в H:i');
                ++$i;
            }
            return response()->json($response);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function stickerSave(Request $request)
    {
        $sticker = new Sticker;
        if (Auth::user()->cannot('create', $sticker)) {
            abort(403);
        }

        $sticker->text = $request->sticker_text;
        $sticker->color = dechex(mt_rand(0x999999, 0xFFFFFF));
        $sticker->author_id = $request->user()->id;

        $sticker->addressee_id = Auth::user()->can('assign', new Sticker) ?
            $request->addressee_id :
            $request->user()->id;

        $this->validate($request, [
            'addressee_id' => 'integer|exists:users,id',
            'sticker_text' => 'string|max:50'
        ]);
        $sticker->save();
        // если себе
        return
            $sticker->author->id == $sticker->addressee_id ?
                response()->json(['action' => 'reload']) :
                response()->json(['action' => 'alert', 'message' => 'Стикер назначен пользователю ' . $sticker->addressee->first_name . '!']);
    }

    /**
     * @param Request $request
     * @param bool $deleted
     * @return \Illuminate\Http\JsonResponse
     */
    public function stickers(Request $request, $deleted = false)
    {
        if (Auth::user()->cannot('read', new Sticker)) {
            abort(403);
        }

        if ($request->isMethod('get')) {

            $result = !$deleted ?
                Sticker::where('addressee_id', $request->user()->id)
                    ->orderBy('created_at', 'desc')->paginate(11) :
                Sticker::onlyTrashed()->where('addressee_id', $request->user()->id)
                    ->orderBy('deleted_at', 'desc')->paginate(11);

            $links = empty($result->links()) ? '' : $result->links()->toHtml();
            $response = ['data' => [],
                'links' => $links,
                'deleted' => $deleted,
            ];

            $i = 0;

            foreach ($result->getCollection() as $sticker) {
                $response['data'][$i]['text'] = $sticker->text;
                $response['data'][$i]['sticker_id'] = $sticker->id;
                $response['data'][$i]['color'] = $sticker->color;
                $response['data'][$i]['author'] = $sticker->author->id == $request->user()->id ? '' : $sticker->author->first_name;
                $response['data'][$i]['date'] = $sticker->created_at->format('d.m.Y');
                ++$i;
            }
            return response()->json($response);
        }
    }

    /**
     * @param Sticker $sticker
     */
    public function removeSticker(Sticker $sticker)
    {
        if (Auth::user()->cannot('delete', $sticker)) {
            abort(403);
        }
        $sticker->delete();
    }

    /**
     * @param $id
     */
    public function restoreSticker($id)
    {
        Sticker::withTrashed()->find($id)->restore();
    }

    /**
     * @param Request $request
     * @param Sticker $sticker
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSticker(Request $request, Sticker $sticker)
    {
        if (Auth::user()->cannot('update', $sticker)) {
            abort(403);
        }

        $sticker->text = $request->sticker_text;

        if ($request->user()->can('assign', $sticker) && $sticker->addressee_id != $request->addressee_id) {
            $sticker->addressee_id = $request->addressee_id;
        }


        $this->validate($request, [
            'addressee_id' => 'integer|exists:users,id',
            'sticker_text' => 'string|max:50'
        ]);
        $sticker->save();
        return response()->json(['action' => 'reload']);
    }
}