<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Candidate;
use App\Participant;

use App\History;

class CandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //вот тут вывод всех именно кандидатов (т.е. тех у кого есть подобранные вакансии)
        // пока не делаем. выводим всех user
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->cannot('create', new Candidate)) {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {

        $candidate = new Candidate;
        if (Auth::user()->cannot('create', $candidate)) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            $candidate->user_id = $request->user_id;
            $candidate->vacancy_id = $request->vacancy_id;
            $candidate->status = $request->interview ? Candidate::STATUS_INTERVIEW : Candidate::STATUS_FOUNDED;
            $this->validate($request, $candidate->getRules());

            if ($request->interview) {
                $this->makeAppointment($request);
            }

            $candidate->save();

            History::storeEntry([
                'manager_id' => $request->user()->id,
                'user_id' => $candidate->user_id,
                'vacancy_id' => $candidate->vacancy_id,
                'old_val' => null,
                'new_val' => $candidate->status,
                'action_type' => History::ACTION_CREATED,
            ]);

        } catch (ValidationException $e) {
            DB::rollback();
            return $e->response;
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        DB::Commit();

        return response()->json([
            'action' => 'added',
            'candidate_id' => $candidate->id,
            'user_id' => $request->user_id,
            'candidate_status' => Candidate::getStatus()[$candidate->status],
            'status_id' => $candidate->status,
            'candidate_class' => Candidate::getCssClass($candidate->status),
            'candidate_name' => $candidate->user->last_name . ' ' . $candidate->user->first_name,
            'candidate_salary' => $candidate->user->salary . ' ' . $candidate->user->dictionaryValue('currency'),
            'updated' => $candidate->updated_at->format('d.m.y в H:i:s'),
            'vacancy_id' => $candidate->vacancy->id,
            'vacancy_title' => $candidate->vacancy->title,
            'vacancy_salary' => $candidate->vacancy->salary . ' ' . $candidate->vacancy->dictionaryValue('currency'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Candidate $candidate
     * @return \Illuminate\Http\Response
     */
    public function show(Candidate $candidate)
    {
        if (Auth::user()->cannot('read', $candidate)) {
            abort(403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Candidate $candidate
     * @return \Illuminate\Http\Response
     */
    public function edit(Candidate $candidate)
    {
        if (Auth::user()->cannot('update', $candidate)) {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Candidate $candidate
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, Candidate $candidate)
    {
        if (Auth::user()->cannot('update', $candidate)) {
            abort(403);
        }

        DB::beginTransaction();
        try {
            $old_class = Candidate::getCssClass($candidate->status);
            $old_val = $candidate->status;
            $candidate->status = $request->status;

            if ($request->interview) {
                $this->makeAppointment($request);
            }

            $candidate->save();

            History::storeEntry([
                'manager_id' => $request->user()->id,
                'user_id' => $candidate->user_id,
                'vacancy_id' => $candidate->vacancy_id,
                'old_val' => $old_val,
                'new_val' => $candidate->status,
                'action_type' => History::ACTION_UPDATED,
            ]);

        } catch (ValidationException $e) {
            DB::rollback();
            return $e->response;
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::Commit();

        return response()->json([
            'action' => 'updated',
            'id' => $candidate->id,
            'old_class' => $old_class,
            'new_class' => Candidate::getCssClass($candidate->status),
            'candidate_status' => Candidate::getStatus()[$candidate->status],
            'status' => $request->status,
        ]);

    }

    /**
     *
     * @param Request $request
     * @param Candidate $candidate
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, Candidate $candidate)
    {
        if (Auth::user()->cannot('delete', $candidate)) {
            abort(403);
        }

        $candidate->delete();

        History::storeEntry([
            'manager_id' => $request->user()->id,
            'user_id' => $candidate->user_id,
            'vacancy_id' => $candidate->vacancy_id,
            'old_val' => null,
            'new_val' => null,
            'action_type' => History::ACTION_DELETED,
        ]);

        return response()->json([
            'status' => 'success',
            'candidate_id' => $candidate->id,
            'vacancy_id' => $candidate->vacancy->id,
            'vacancy_title' => $candidate->vacancy->title,
        ]);
    }

    /**
     * @param Request $request
     */
    public function makeAppointment(Request $request)
    {
        $this->validate($request, Event::getRules($request->start, $request->end));
        $event = Event::create([
            'start' => strtotime($request->start),
            'end' => strtotime($request->end),
            'manager_id' => $request->user()->id,
            'title' => 'Cобеседование',
            'color' => $request->color,
            'description' => $request->description,
        ]);
        Participant::storeParticipants($event, $request->participants ? $request->participants : []);
    }
}
