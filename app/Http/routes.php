<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('/organizer');
});

// Authentication Routes...
$this->get('login', 'Auth\AuthController@showLoginForm');
$this->post('login', 'Auth\AuthController@login');
$this->get('logout', 'Auth\AuthController@logout');

// Registration Routes...
//$this->get('register', 'Auth\AuthController@showRegistrationForm');
//$this->post('register', 'Auth\AuthController@register');

// Password Reset Routes...
$this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
$this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
$this->post('password/reset', 'Auth\PasswordController@reset');

//Route::get('/home', 'HomeController@index');


Route::group(['middleware' => 'auth'], function () {


    Route::get('download/{user}', ['as' => 'download', 'uses' => 'UserController@download']);
    Route::get('users', ['as' => 'users', 'uses' => 'UserController@index']);
    Route::get('user/{user}/history', ['as' => 'user.history', 'uses' => 'UserController@history']);
    Route::post('user/upload', ['as' => 'user.upload', 'uses' => 'UserController@upload']);
    Route::get('user/upload', ['as' => 'user.upload', 'uses' => 'UserController@upload']);

    Route::get('user/test', ['as' => 'user.test', 'uses' => 'UserController@parseForm']);
    Route::get('user/parse', ['as' => 'user.parse', 'uses' => 'UserController@parseTest']);

    Route::resource('user', 'UserController');


    //Route::put();
    Route::get('vacancy/{vacancy}/history', ['as' => 'vacancy.history', 'uses' => 'VacancyController@history']);
    Route::get('vacancies', ['as' => 'vacancies', 'uses' => 'VacancyController@index']);
    Route::resource('vacancy', 'VacancyController');


    Route::get('dictionary/toggle', ['as' => 'dictionary.toggle', 'uses' => 'DictionaryController@toggle']);
    Route::resource('dictionary', 'DictionaryController');


    Route::get('companies', ['as' => 'companies', 'uses' => 'CompanyController@index']);
    Route::resource('company', 'CompanyController');
    Route::resource('company', 'CompanyController');


    Route::get('events', ['as' => 'events', 'uses' => 'EventController@index']);
    Route::resource('event', 'EventController');
    Route::get('organizer', ['as' => 'organizer', 'uses' => 'OrganizerController@index']);
    Route::get('organizer/history', ['as' => 'organizer.history', 'uses' => 'OrganizerController@history']);
    Route::get('organizer/events', ['as' => 'organizer.events', 'uses' => 'OrganizerController@events']);


    Route::get('organizer/stickers/{deleted?}', ['as' => 'organizer.stickers', 'uses' => 'OrganizerController@stickers']);
    Route::post('organizer/sticker/store', ['as' => 'sticker.store', 'uses' => 'OrganizerController@stickerSave']);
    Route::put('organizer/sticker/{sticker}', ['as' => 'sticker.update', 'uses' => 'OrganizerController@updateSticker']);
    Route::post('organizer/sticker/{sticker}', ['as' => 'sticker.restore', 'uses' => 'OrganizerController@restoreSticker']);
    Route::delete('organizer/sticker/{sticker}', ['as' => 'sticker.remove', 'uses' => 'OrganizerController@removeSticker']);


    Route::post('candidate/{user}', ['as' => 'setvacancy', 'uses' => 'UserController@setVacancy']);
    Route::resource('candidate', 'CandidateController');


});


