<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    const ACTION_CREATED = 1;
    const ACTION_UPDATED = 2;
    const ACTION_DELETED = 3;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'manager_id',
        'user_id',
        'vacancy_id',
        'old_val',
        'new_val',
        'action_type',
    ];

    /**
     * @param array $data
     */
    public static function storeEntry(array $data)
    {
        // add custom validation

        $history = new self;
        $history->manager_id = $data['manager_id'];
        $history->user_id = $data['user_id'];
        $history->vacancy_id = $data['vacancy_id'];
        $history->old_val = $data['old_val'];
        $history->new_val = $data['new_val'];
        $history->action_type = $data['action_type'];

        $history->save();
    }

    /**
     * @param bool $id
     */
    public function getHistory($id = false)
    {

    }

    /**
     * @return mixed
     */
    public function manager()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * @return mixed
     */
    public function vacancy()
    {
        return $this->belongsTo(Vacancy::class)->withTrashed();
    }

    /**
     * @return array
     */
    public static function getActionType()
    {
        static $types = [];
        if (empty($types)) {
            $types=[
                self::ACTION_CREATED => 'Добавил(-а)',
                self::ACTION_UPDATED => 'Изменил(-а)',
                self::ACTION_DELETED => 'Удалил(-ла)',
            ];
        }
        return $types;
    }
}
