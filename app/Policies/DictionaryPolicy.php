<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Dictionary;

class DictionaryPolicy
{
    use HandlesAuthorization;

    public function create(User $user, Dictionary $model)
    {
        return $user->allowed('Dictionary@create');
    }

    public function read(User $user, Dictionary $model)
    {
        return $user->allowed('Dictionary@read');
    }

    public function update(User $user, Dictionary $model)
    {
        return $user->allowed('Dictionary@update');
    }

    public function delete(User $user, Dictionary $models)
    {
        return $user->allowed('Dictionary@delete');
    }
}
