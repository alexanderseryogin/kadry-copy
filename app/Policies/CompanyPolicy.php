<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Company;

class CompanyPolicy
{
    use HandlesAuthorization;

    public function create(User $user, Company $model)
    {
        return $user->allowed('Company@create');
    }

    public function read(User $user, Company $model)
    {
        return $user->allowed('Company@read');
    }

    public function update(User $user, Company $model)
    {
        return $user->allowed('Company@update');
    }

    public function delete(User $user, Company $models)
    {
        return $user->allowed('Company@delete');
    }
}
