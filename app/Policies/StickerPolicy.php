<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Sticker;

class StickerPolicy
{
    use HandlesAuthorization;

    public function create(User $user, Sticker $model)
    {
        return $user->allowed('Sticker@create');
    }

    public function read(User $user, Sticker $model)
    {
        return $user->allowed('Sticker@read');
    }

    public function update(User $user, Sticker $model)
    {
        return $user->allowed('Sticker@update');
    }

    public function delete(User $user, Sticker $model)
    {
        return $user->allowed('Sticker@delete');
    }

    public function assign(User $user, Sticker $model)
    {
        return $user->allowed('Sticker@assign');
    }
}