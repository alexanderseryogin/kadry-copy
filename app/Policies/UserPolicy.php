<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;

class UserPolicy
{
    use HandlesAuthorization;

    public function create(User $user, User $model)
    {
        return $user->allowed('User@create');
    }

    public function read(User $user, User $model)
    {
        return $user->allowed('User@read');
    }

    public function update(User $user, User $model)
    {
        return $user->allowed('User@update') || $model->id == $user->id;
    }

    public function delete(User $user, User $model)
    {
        return $user->allowed('User@delete') || $model->id == $user->id;
    }
}