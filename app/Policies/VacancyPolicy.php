<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Vacancy;

class VacancyPolicy
{
    use HandlesAuthorization;

    public function create(User $user, Vacancy $model)
    {
        return $user->allowed('Vacancy@create');
    }

    public function read(User $user, Vacancy $model)
    {
        return $user->allowed('Vacancy@read');
    }

    public function update(User $user, Vacancy $model)
    {
        return $user->allowed('Vacancy@update');
    }

    public function delete(User $user, Vacancy $models)
    {
        return $user->allowed('Vacancy@delete');
    }
}
