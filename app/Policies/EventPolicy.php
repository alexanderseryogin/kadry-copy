<?php

namespace App\Policies;

use App\Event;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventPolicy
{
    use HandlesAuthorization;

    public function create(User $user, Event $model)
    {
        return $user->allowed('Event@create');
    }

    public function read(User $user, Event $model)
    {
        return $user->allowed('Event@read') &&
        ($model->manager_id == $user->id || boolval($model->participants->where('user_id', $user->id)->count()));
    }

    public function update(User $user, Event $model)
    {
        return $user->allowed('Event@update') && $model->manager_id == $user->id;
    }

    public function delete(User $user, Event $model)
    {
        return $user->allowed('Event@delete') && $model->manager_id == $user->id;
    }
}
