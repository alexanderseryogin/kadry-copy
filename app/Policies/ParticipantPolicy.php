<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Participant;

class ParticipantPolicy
{
    use HandlesAuthorization;

    public function create(User $user, Participant $model)
    {
        return $user->allowed('Participant@create');
    }

    public function read(User $user, Participant $model)
    {
        return $user->allowed('Participant@read');
    }

    public function update(User $user, Participant $model)
    {
        return $user->allowed('Participant@update');
    }

    public function delete(User $user, Participant $models)
    {
        return $user->allowed('Participant@delete');
    }
}
