<?php

namespace App\Policies;

use App\Candidate;
use App\Http\Requests\Request;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;

class CandidatePolicy
{
    use HandlesAuthorization;

    public function create(User $user, Candidate $model)
    {
        return $user->allowed('Candidate@create');
    }

    public function read(User $user, Candidate $model)
    {
        return $user->allowed('Candidate@read');
    }

    public function update(User $user, Candidate $model)
    {
        return $user->allowed('Candidate@update');
    }

    public function delete(User $user, Candidate $models)
    {
        return $user->allowed('Candidate@delete');
    }
}
