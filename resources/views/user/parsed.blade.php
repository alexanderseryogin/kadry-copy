@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="{!! asset('css/bootstrap-datetimepicker.min.css') !!}">
@endsection

@section('content')
    <div class="row">
        <div class="container-fluid">
            <div class="col-md-6">
                <div class="col-md-11 col-md-offset-1">
                    @include('layouts.user-create')
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-11">
                    <div class="panel panel-primary add-shadows">
                        <div class="panel-heading">Исходный текст резюме:</div>
                        <div class="panel-body">
                            <pre>
                                {{$parsed}}
                            </pre>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{!! asset('js/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('js/bootstrap-datetimepicker.min.js') !!}"></script>
    <script src="{!! asset('js/init-birthday.js') !!}"></script>
@endsection
