@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="{!! asset('css/bootstrap-datetimepicker.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap-select.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap-colorpicker.min.css') !!}">
@endsection
@section('content')
    <div class="row">
        @if(session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
        @endif
    </div>

    <div class="row">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-3 col-md-offset-1">
                            <div class="row text-center">
                                <strong>
                                    Источник резюме:
                                </strong>
                                <div class="row text-center">
                                    <a href="{{$user->cv_src}}">
                                        {{$user->cv_src}}
                                    </a>
                                </div>
                            </div>
                        </div><!--CV_SRC-->
                        <div class="col-md-4">
                            <a href="/user/{{$user->id}}/edit">
                                <div class="progress add-shadows">
                                    <div aria-valuemax="100" aria-valuemin="0"
                                         aria-valuenow="{{$user->getCompleteness()}}"
                                         class="progress-bar progress-bar-{{$user->getCompleteness('class')}} progress-bar-striped active"
                                         role="progressbar" style="width: {{$user->getCompleteness()}}%;">
                                        {{$user->getCompleteness()}}%
                                    </div>
                                </div>
                            </a>
                        </div> <!--Progress Bar-->
                        {{--<div class="col-md-4">--}}
                        {{--<div class="pull-right">--}}
                        {{--<span class="label label-info panding">Пассивен</span>--}}
                        {{--</div>--}}
                        {{--</div> <!--Status-->--}}
                    </div>
                    <hr>
                </div><!--Progress Bar, Status, CV_SRC-->
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-4  text-right">
                            <strong>
                                {{$user->first_name}} {{$user->last_name}}
                            </strong> <!--User name-->
                        </div>
                        <div class="col-md-8 text-right">
                            @can('update', $user)
                                <a class="btn btn-primary add-shadows" href="/user/{{$user->id}}/edit">Редактировать</a>
                            @endcan
                            <a class="btn btn-default add-shadows" href="{{ route('users') }}">Назад</a>
                        </div> <!--Edit/back-->
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    Желаемая должность:
                                </p>
                            </div>
                        </div>
                        <div class="col-md-offset-1 col-md-7 text-left">{{$user->dictionaryValue('position')}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    Дата рождения:
                                </p>
                            </div>
                        </div>
                        <div class="col-md-offset-1 col-md-7 text-left">{{$user->birthday}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    Город:
                                </p>
                            </div>
                        </div>
                        <div class="col-md-offset-1 col-md-7 text-left">{{$user->dictionaryValue('residence')}}</div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    <img src="{{asset('icon/email.png')}}" alt="{{asset('icon/email.png')}}">
                                </p>
                            </div>
                        </div>
                        <div class="col-md-offset-1 col-md-7 text-left">{{$user->email}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    <img src="{{asset('icon/phone.png')}}" alt="{{asset('icon/phone.png')}}">
                                </p>
                            </div>
                        </div>
                        <div class="col-md-offset-1 col-md-7 text-left">{{$user->phone}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    <img src="{{asset('icon/skype.png')}}" alt="{{asset('icon/skype.png')}}">
                                </p>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-7 text-left">{{$user->skype}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    <img src="{{asset('icon/linkedin.png')}}" alt="{{asset('icon/linkedin.png')}}">
                                </p>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-7 text-left">{{$user->linkedin}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    <img src="{{asset('icon/fb.png')}}" alt="{{asset('icon/fb.png')}}">
                                </p>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-7 text-left">{{$user->facebook}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    <img src="{{asset('icon/vk.png')}}" alt="{{asset('icon/vk.png')}}">
                                </p>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-7 text-left">{{$user->vk}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    Другое:
                                </p>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-7"></div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    Владение языками:
                                </p>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-7 text-left">{{$user->lang}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    Зарплатные ожидания:
                                </p>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-7 text-left">{{$user->salary}} {{$user->dictionaryValue('currency')}}</div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    Опыт работы:
                                </p>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-7 text-left">{{$user->company->title}}</div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    Образование:
                                </p>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-7 text-left">{{$user->education}}</div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 text-right">
                            <div class="tip">
                                <p class="subscription">
                                    Дополнительная информация:
                                </p>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-7 text-left">
                            {{$user->about}}
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="center-block img img-thumbnail center-block user-avatar add-shadows"
                         style="background-image: url('{{$user->img_name? asset('uploads/img/'.$user->id):asset('icon/avatar_dummy.jpg')}}')"></div>

                    @if($user->cv_name)
                        <div class="resume">
                            <a href="{{route('download', ['id' => $user->id])}}"
                               class="btn btn-primary center-block add-shadows"
                               id="download">
                                <i class="fa fa-download" aria-hidden="true"></i> Скачать резюме
                            </a>
                        </div>
                    @endif

                    @can('create', new \App\Candidate)
                        <div class="vacancies ">
                            <a type="button" class="btn btn-success center-block user-single-button add-shadows"
                               data-toggle="modal"
                               data-target="#vacancy-select">
                                Подобрать вакансии
                            </a>
                        </div>
                    @endcan
                    <div class="text-center">
                        ПОДОБРАННЫЕ ВАКАНСИИ:
                    </div>
                    <div class="items-list candidates">
                        <table class="table table-bordered table-hover candidates-table add-shadows">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th>З/П</th>
                                <th>Статус</th>
                                @can('delete', new \App\Candidate)
                                    <th><i class="fa fa-trash" aria-hidden="true"></i></th>
                                @endcan
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($candidate as $vacancy)
                                @can('read', $vacancy)
                                    <tr id="candidate-{{$vacancy->id}}">
                                        <td>
                                            <a class="item"
                                               href="{{route('vacancy.show', ['id'=>$vacancy->vacancy->id])}}">
                                                {{$vacancy->vacancy->title}}
                                            </a>
                                        <td class="text-center">{{$vacancy->vacancy->salary}} {{$vacancy->vacancy->dictionaryValue('currency')}}</td>
                                        <td class="text-center">
                                            @can('update', $vacancy)
                                                <button class="btn btn-xs status-label add-shadows {{\App\Candidate::getCssClass($vacancy->status)}}"
                                                        data-status="{{$vacancy->status}}">
                                                    {{\App\Candidate::getStatus()[$vacancy->status]}}
                                                </button>
                                            @endcan
                                            @cannot('update', $vacancy)
                                                <label class="btn btn-xs status-label add-shadows {{\App\Candidate::getCssClass($vacancy->status)}}">
                                                    {{\App\Candidate::getStatus()[$vacancy->status]}}
                                                </label>
                                                @endcan
                                        </td>
                                        @can('delete', $vacancy)
                                            <td class="text-center">

                                                {{Form::open([ 'method'  => 'delete', 'class'=>'delete-form', 'route' => [ 'candidate.destroy', $vacancy->id ]])}}
                                                {!!Form::button('<i class="fa fa-btn fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                                                {!!Form::close()!!}
                                            </td>
                                        @endcan
                                    </tr>
                                @endcan
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="container-fluid">
                    <div class="row">
                        <h3 class="text-center">История изменений:</h3>
                    </div>
                    <div class="row">
                        <div class="container items-list">
                            <table class="table table-striped table-responsive history add-shadows">
                                <thead>
                                <th>Ответственный</th>
                                <th>Действие</th>
                                <th>Вакансия</th>
                                <th>Старый статус</th>
                                <th>Новый статус</th>
                                <th class="text-right">Дата</th>
                                </thead>
                                <tbody id="history_entries">
                                </tbody>
                            </table>
                            <div class="container pagination-container"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @can('create', new \App\Candidate)
        <!-- Modal For Candidate Select-->
        @include('layouts.vacancy-select', [
        'modal_id'=>'vacancy-select',
        'modal_title'=>'Выберите из списка вакансию:',
        'action'=>'CandidateController@store',
        'action_id'=>null,
        'method'=>'post',
        'start'=>'start-1',
        'end'=>'end-1',
        'color'=>'event-color-1'
        ])
    @endcan

    @can('update', new \App\Candidate)
        <!-- Modal For Status Change-->
        @include('layouts.status-select', [
        'modal_id'=>'status-select',
        'modal_title'=>'Изменить статус для кандидата',
        //'action'=>'CandidateController@update',
        'action'=>'CandidateController@update',
        'action_id'=>0,
        'method'=>'put',
        'start'=>'start-2',
        'end'=>'end-2',
        'color'=>'event-color-2'
        ])
    @endcan

@endsection

@section('scripts')
    @can('create', new \App\Candidate)
        <script src="{{asset('js/user_candidate_ajax.js')}}"></script>
        <script src="{!! asset('js/moment-with-locales.min.js') !!}"></script>
        <script src="{!! asset('js/bootstrap-datetimepicker.min.js') !!}"></script>
        <script src="{!! asset('js/bootstrap-select.min.js') !!}"></script>
        <script src="{!! asset('js/bootstrap-colorpicker.min.js') !!}"></script>
        <script src="{!! asset('js/event_create.js') !!}"></script>
    @endcan
    <script src="{!! asset('js/single_history.js') !!}"></script>
@endsection





