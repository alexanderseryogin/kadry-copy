@extends('layouts.app')

@section('content')
    {{--@include('employees.add_button')--}}
    <div class="row">
        @if(session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
        @endif
    </div>

    {{--<div class="row  search-bar">--}}
        {{--<div class="container-fluid">--}}
            {{--<div class="col-lg-10 col-lg-offset-1">--}}

                {{--<div class="input-group">--}}
                    {{--<span class="input-group-btn">--}}
                        {{--<a class="btn btn-info" href="#">--}}
                            {{--<i class="glyphicon glyphicon-search"></i>--}}
                        {{--</a>--}}
                    {{--</span>--}}
                    {{--<input class="form-control" placeholder="Search for..." type="text">--}}
                {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="row">
        <div class="container-fluid">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="items-list">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Кандидат</th>
                            <th>Должность</th>
                            <th>Регион</th>
                            <th>Зарплата</th>
                            <th class="pending">Ответственный</th>
                            <th>Добавлен</th>
                            <th class="pending">Статус</th>
                            <th>Примечание</th>
                            <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                            <th><i class="fa fa-trash" aria-hidden="true"></i></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                    <a class="item" id="profile-name"
                                       href="{{route('user.show', ['id' => $user->id])}}">
                                        {{$user->last_name}} {{$user->first_name}}
                                    </a>
                                </td>
                                <td>{{$user->dictionaryValue('position')}}</td>
                                <td>{{$user->dictionaryValue('residence')}}</td>
                                <td>{{$user->salary}} {{$user->dictionaryValue('currency')}}</td>
                                <td class="pending">Pending</td>
                                <td>{{$user->created_at}}</td>
                                <td class="pending">Pending</td>
                                <td>{{$user->notes}}</td>
                                <td>
                                    @can('update', $user)
                                        {{Form::open([ 'method'  => 'get', 'route' => [ 'user.edit', $user->id ]])}}
                                        {!!Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                                        {!!Form::close()!!}
                                    @endcan
                                </td>
                                <td>
                                    @can('delete', $user)
                                        {{Form::open([ 'method'  => 'delete', 'class'=>'delete-form','route' => [ 'user.destroy', $user->id ]])}}
                                        {!!Form::button('<i class="fa fa-btn fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                                        {!!Form::close()!!}
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/confirm_delete.js')}}"></script>
@endsection