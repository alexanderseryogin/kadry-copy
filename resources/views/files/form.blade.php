@extends('layouts.app')

@section('content')
    <div class="container">
        @if(count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $e)
                        <li>{{$e}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary add-shadows">
                    <div class="panel-heading">Регистрация</div>
                    <div class="panel-body">
                        {!!Form::open(['action'=>'UserController@parseTest', 'method'=>'get', 'class'=>'form-horizontal'])!!}


                        <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">
                            {!!Form::label('text', 'Текст резюме: ', ['class'=>'col-md-4 control-label requierd'])!!}
                            <div class="col-md-6">
                                {!!Form::textarea('text', old('text'), ['class'=>'form-control'])!!}
                                @if ($errors->has('text'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('text') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                {!!Form::button('<i class="fa fa-btn fa-user"></i> Проверка', ['type'=>'submit', 'class'=>'btn btn-primary pull-right'])!!}
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/parse_ajax')}}"></script>
@endsection
