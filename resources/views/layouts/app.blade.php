<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    @if(isset($page))
        <meta name="page" content="{{$page}}">
    @endif
    <title>Faceit-HRM</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"
          integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap-theme.min.css') !!}">


    @yield('styles')
    <link rel="stylesheet" href="{!! asset('css/app.css') !!}">


    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>

</head>
<body id="app-layout">
<nav class="navbar navbar-inverse navbar-static-top">

    <div class="container-fluid navbar-container add-shadows">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Faceit-HRM
                </a>
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                @if(Auth::check())
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="{{route('organizer')}}" class="{{$page=='organizer'?'current':''}}">Органайзер</a>
                            <a href="{{route('events')}}" class="{{$page=='events'?'current':''}}"><i
                                        class="fa fa-calendar" aria-hidden="true"></i></a>
                        </li>
                        @can('read', new \App\Vacancy)
                            <li>
                                <a href="{{route('vacancies')}}"
                                   class="{{$page=='vacancies'?'current':''}}">Вакансии</a>
                                <span class="sr-only"></span>
                                @can('create', new \App\Vacancy)
                                    <a id="add-vacancy" href="{{route('vacancy.create')}}"
                                       class="{{$page=='vacancy-edit'?'current':''}}">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                @endcan
                            </li>
                        @endcan
                        @can('read', new \App\User)
                            <li>
                                <a href="{{route('users')}}" class="{{$page=='users'?'current':''}}">Кандидаты</a>
                                @can('create', new \App\User)
                                    <a class="dropdown-toggle {{$page=='user-edit'?'current':''}}" aria-expanded="false"
                                       aria-haspopup="false"
                                       data-toggle="dropdown"
                                       role="button" id="add-candidate" href="#">
                                        <i class="fa fa-th-list" aria-hidden="true"></i>
                                        <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <a class="dropdown-toggle" aria-expanded="false" aria-haspopup="false"
                                           data-toggle="dropdown" role="button" style="color:#ddd;"
                                           id="add-candidate" href="#">
                                        </a>
                                        <li>
                                            <a id="add-candidate" href="{{route('user.create')}}">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                Добавить вручную
                                            </a>
                                        </li>
                                        <li>
                                            <a class="resume_upload" id="upload">
                                                <i class="glyphicon glyphicon-download-alt"></i>
                                                Загрузить из файла
                                            </a>
                                            {!!Form::open(['action'=>'UserController@upload', 'method'=>'post', 'files'=>true, 'hidden'=>'hidden'])!!}
                                            {!!Form::file('cv', null, [])!!}
                                            {!! Form::close() !!}
                                        </li>
                                    </ul>
                                @endcan
                            </li>
                        @endcan
                        @can('read', new \App\Company)
                            <li>
                                <a href="{{route('companies')}}"
                                   class="{{$page=='companies'?'current':''}}">Компании</a>
                                @can('create', new \App\Company)
                                    <a id="add-company" href="{{route('company.create')}}"
                                       class="{{$page=='company-edit'?'current':''}}">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                @endcan
                            </li>
                        @endcan
                        @can('read', new \App\Dictionary)
                            <li>
                                <a href="{{route('dictionary.index')}}" class="{{$page=='dictionary'?'current':''}}">Словарь</a>
                                @can('create', new \App\Dictionary)
                                    <a id="add-company" href="{{route('dictionary.create')}}"
                                       class="{{$page=='dictionary-edit'?'current':''}}">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                @endcan
                            </li>
                        @endcan
                    </ul>
            @endif
            <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Вход</a>
                        </li>
                        {{--<li><a href="{{ url('/register') }}"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Регистрация</a>--}}
                        {{--</li>--}}
                    @else
                        <li>
                            <a href="{{ url('/user').'/'. Auth::user()->id}}"
                               class="img img-thumbnail nav-avatar add-shadows"
                               style="background-image: url('{{Auth::user()->img_name? asset('uploads/img/'.Auth::user()->id):asset('icon/avatar_dummy.jpg')}}')">
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/user').'/'. Auth::user()->id}}">
                                <i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{Auth::user()->email}}
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/logout') }}">
                                <i class="fa fa-power-off" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Выйти
                            </a>
                        </li>

                        {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">--}}
                        {{--{{ Auth::user()->name }} <span class="caret"></span>--}}
                        {{--</a>--}}

                        {{--<ul class="dropdown-menu" role="menu">--}}
                        {{--<li><a href="{{ url('/logout') }}"><i class="glyphicon glyphicon-off"></i>Выйти</a></li>--}}
                        {{--</ul>--}}
                        {{--</li>--}}
                    @endif
                </ul>
            </div>
        </div>
    </div>


</nav>

@yield('content')

<!-- JavaScripts -->

<script src="{!! asset('js/jquery.min.js') !!}"></script>
<script src="{!! asset('js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('js/upload_cv.js') !!}"></script>

@yield('scripts')
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
