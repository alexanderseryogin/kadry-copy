<div class="panel panel-primary add-shadows">
    <div class="panel-heading">Регистрация</div>
    <div class="panel-body">
        {!!Form::model($user,['action'=>[$action, $user->id], 'method'=>$method, 'files'=>true, 'class'=>'form-horizontal'])!!}

        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
            {!!Form::label('last_name', 'Фамилия: ', ['class'=>'col-md-4 control-label required'])!!}
            <div class="col-md-6">
                {!!Form::text('last_name', old('last_name'), ['class'=>'form-control required'])!!}
                @if ($errors->has('last_name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
            {!!Form::label('first_name', 'Имя: ', ['class'=>'col-md-4 control-label required'])!!}
            <div class="col-md-6">
                {!!Form::text('first_name', old('first_name'), ['class'=>'form-control'])!!}
                @if ($errors->has('first_name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('patronymic') ? ' has-error' : '' }}">
            {!!Form::label('patronymic', 'Отчество: ', ['class'=>'col-md-4 control-label'])!!}
            <div class="col-md-6">
                {!!Form::text('patronymic', old('patronymic'), ['class'=>'form-control'])!!}
                @if ($errors->has('patronymic'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('patronymic') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('birthday')? ' has-error' : '' }}">
            {!!Form::label('birthday', 'Дата рождения: ', ['class'=>'col-md-4 control-label'])!!}
            <div class="col-md-6">
                <div class='input-group date start' id='b-day'>
                    {!!Form::text('birthday', old('birthday'), ['class'=>'form-control'])!!}
                    <span class="input-group-addon"><span
                                class="glyphicon glyphicon-calendar"></span></span>

                </div>
                @if ($errors->has('birthday'))
                    <div class="row">
                        <div class="container">
                                        <span class="help-block">
                                                <strong>{{ $errors->first('birthday') }}</strong>
                                            </span>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('position_id') ? ' has-error' : '' }}">
            {!!Form::label('position_id', 'Желаемая должность: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::select('position_id', $positions, old('position_id'), ['class'=>'form-control', 'placeholder'=>'Выберите должность'])!!}
                @if ($errors->has('position_id'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('salary') ? ' has-error' : '' }}">
            {!!Form::label('salary', 'Желаемая ЗП: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-7">
                        {!!Form::number('salary', old('salary'), ['class'=>'form-control'])!!}
                    </div>
                    <div class="col-md-5">
                        {!!Form::select('currency_id', $currencies, old('currency'), ['class'=>'form-control', 'placeholder'=>'Выберите валюту'])!!}
                    </div>
                </div>

                @if ($errors->has('salary'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('salary') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('residence_id') ? ' has-error' : '' }}">
            {!!Form::label('residence_id', 'Место проживания: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::select('residence_id', $residences, old('residence_id'), ['class'=>'form-control', 'placeholder'=>'Выберите город'])!!}
                @if ($errors->has('residence_id'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('residence_id') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
            {!!Form::label('location_id', 'Ищу работу в: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::select('location_id', $locations, old('location_id'), ['class'=>'form-control', 'placeholder'=>'Выберите город'])!!}
                @if ($errors->has('location_id'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('location_id') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('relocatable') ? ' has-error' : '' }}">
            {!!Form::label('relocatable', 'Готов к переезду: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {{Form::checkbox('relocatable', old('relocatable'), old('relocatable'))}}
                {{--{!!Form::text('relocatable', old('relocatable'), ['class'=>'form-control'])!!}--}}
                @if ($errors->has('relocatable'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('relocatable') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('cv_src') ? ' has-error' : '' }}">
            {!!Form::label('cv_src', 'Источник резюме: ', ['class'=>'col-md-4 control-label required'])!!}
            <div class="col-md-6">
                {!!Form::text('cv_src', old('cv_src'), ['class'=>'form-control'])!!}
                @if ($errors->has('cv_src'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('cv_src') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('education') ? ' has-error' : '' }}">
            {!!Form::label('education', 'Образование: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::text('education', old('education'), ['class'=>'form-control'])!!}
                @if ($errors->has('name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('education') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('lang') ? ' has-error' : '' }}">
            {!!Form::label('lang', 'Языки: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::text('lang', old('lang'), ['class'=>'form-control'])!!}
                @if ($errors->has('lang'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('lang') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            {!!Form::label('email', 'Почта: ', ['class'=>'col-md-4 control-label required'])!!}
            <div class="col-md-6">
                {!!Form::email('email', old('email'), ['class'=>'form-control'])!!}
                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
            {!!Form::label('phone', 'Телефон: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::text('phone', old('name'), ['class'=>'form-control'])!!}
                @if ($errors->has('phone'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('skype') ? ' has-error' : '' }}">
            {!!Form::label('skype', 'Skype: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::text('skype', old('name'), ['class'=>'form-control'])!!}
                @if ($errors->has('skype'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('skype') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('user_links') ? ' has-error' : '' }}">
            {!!Form::label('user_links', 'Мои странички: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::text('user_links', old('user_links'), ['class'=>'form-control'])!!}
                @if ($errors->has('user_links'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('user_links') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('company_id') ? ' has-error' : '' }}">
            {!!Form::label('company_id', 'Компания: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::select('company_id', $companies, old('company_id'), ['class'=>'form-control', 'placeholder'=>'Выберите компанию'])!!}
                @if ($errors->has('company_id'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('company_id') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('linkedin') ? ' has-error' : '' }}">
            {!!Form::label('linkedin', 'Linkedin: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::text('linkedin', old('linkedin'), ['class'=>'form-control'])!!}
                @if ($errors->has('name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('linkedin') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
            {!!Form::label('facebook', 'Facebook: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::text('facebook', old('facebook'), ['class'=>'form-control'])!!}
                @if ($errors->has('facebook'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('facebook') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('vk') ? ' has-error' : '' }}">
            {!!Form::label('vk', 'Vkontakte: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::text('vk', old('vk'), ['class'=>'form-control'])!!}
                @if ($errors->has('name'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('vk') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('google_plus') ? ' has-error' : '' }}">
            {!!Form::label('google_plus', 'Google+: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::text('google_plus', old('google_plus'), ['class'=>'form-control'])!!}
                @if ($errors->has('google_plus'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('google_plus') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
            {!!Form::label('notes', 'Примечание: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::text('notes', old('name'), ['class'=>'form-control'])!!}
                @if ($errors->has('notes'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('notes') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('about') ? ' has-error' : '' }}">
            {!!Form::label('about', 'Описание: ', ['class'=>'col-md-4 control-label requierd'])!!}
            <div class="col-md-6">
                {!!Form::textarea('about', old('about'), ['class'=>'form-control'])!!}
                @if ($errors->has('about'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('cv_name') ? ' has-error' : '' }}">
            <div class="col-md-offset-4 col-md-6">
                {!!Form::label('img_name', 'Добавить фото', ['class'=>'btn btn-block btn-default'])!!}
                {!!Form::file('img_name', null, ['class'=>'btn-block btn-primary'])!!}
            </div>
        </div>

        <div class="form-group{{ $errors->has('cv_name') ? ' has-error' : '' }}">
            <div class="col-md-offset-4 col-md-6">
                {!!Form::label('cv_name', 'Добавить резюме', ['class'=>'btn btn-block btn-default'])!!}
                {!!Form::file('cv_name', null, ['class'=>'btn-block btn-primary'])!!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <a href="{{route('users')}}" class="btn btn-default">
                    <i class="fa fa-backward" aria-hidden="true"></i> Назад
                </a>
                {!!Form::button('<i class="fa fa-btn fa-user"></i> Создать', ['type'=>'submit', 'class'=>'btn btn-primary pull-right'])!!}
            </div>
        </div>

        {!!Form::close()!!}

    </div>
</div>