@extends('layouts.modal')

@section($modal_id)
    {!! Form::hidden('vacancy_id', $vacancy->id)!!}
    <div class="form-group{{ $errors->has('user') ? ' has-error' : '' }}  col-lg-12">
        {!!Form::label('user_id', 'Кандидаты', ['class'=>'col-md-4 control-label class add-candidates-modal'])!!}
        <div class="col-md-8 users-select">
            {!!Form::select('user_id', $users, old('user'), ['class'=>'form-control'])!!}
            @if ($errors->has('user'))
                <span class="help-block">
                <strong>{{ $errors->first('user') }}</strong>
            </span>
            @endif
        </div>
    </div>
@endsection