@extends('layouts.modal')

@section($modal_id)
    {!! Form::hidden('user_id', $user->id)!!}
    <div class="form-group{{ $errors->has('users') ? ' has-error' : '' }}  col-lg-12">
        {!!Form::label('vacancy_id', 'Вакансии', ['class'=>'col-md-4 control-label class add-candidates-modal'])!!}
        <div class="col-md-8 users-select">
            {!!Form::select('vacancy_id', $vacancies, old('vacancy_id'), ['class'=>'form-control'])!!}
            @if ($errors->has('vacancy_id'))
                <span class="help-block">
                <strong>{{ $errors->first('vacancy_id') }}</strong>
            </span>
            @endif
        </div>
    </div>
@endsection