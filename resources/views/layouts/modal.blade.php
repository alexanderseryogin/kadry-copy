<div class="modal fade" id="{{$modal_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">{{$modal_title}} <span class="modal-item"></span></h4>
            </div>

            {!! Form::open(['action'=>[$action, $action_id], 'method'=>$method, 'class'=>'modal-form', 'data-method'=>$method])!!}

            <div class="modal-body">
                <div class="row">
                    @yield($modal_id)
                    <div class="form-group {{ $errors->has('interview') ? ' has-error' : '' }}  col-lg-12 inteview-checker">
                        {!!Form::label('interview', 'Назначить собеседование?', ['class'=>'col-md-4 control-label class'])!!}
                        <div class="col-md-8 users-select">
                            {{Form::checkbox('interview', null, null, ['class'=>'appoint-checkbox'])}}
                            @if ($errors->has('interview'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('interview') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row interview-row">
                    {!! Form::hidden('title', 'Собеседование')!!}
                    <div class="form-group{{ $errors->has('event_color') ? ' has-error' : '' }} col-lg-12">
                        {!!Form::label('color', 'Цвет события: ', ['class'=>'col-md-4 control-label'])!!}
                        <div class="col-md-8">
                            <div id="{{$color}}" class="input-group colorpicker-component">
                                {!!Form::text('color', '#3399f3', ['class'=>'form-control'])!!}
                                <span class="input-group-addon"><i></i></span>
                            </div>
                            @if ($errors->has('color'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('color') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}  col-lg-12">
                        {!!Form::label('description', 'Описание: ', ['class'=>'col-md-4 control-label'])!!}
                        <div class="col-md-8">
                            {!!Form::textarea('description', null, ['class'=>'form-control'])!!}
                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    @can('create', new \App\Participant)
                    <div class="form-group{{ $errors->has('participants') ? ' has-error' : '' }}  col-lg-12">
                        {!!Form::label('participants', 'Участники: ', ['class'=>'col-md-4 control-label'])!!}
                        <div class="col-md-8">
                            {!!Form::select('participants[]', $participants, old('event_participants[]'), ['class'=>'form-control selectpicker', 'multiple' => 'multiple', "data-live-search"=>"true"])!!}
                            @if ($errors->has('participants'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('participants') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    @endcan

                    <div class="form-group{{ $errors->has('start') || $errors->has('end') ? ' has-error' : '' }}  col-lg-12">
                        {!!Form::label('start', 'Время: ', ['class'=>'col-md-4 control-label requierd'])!!}
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class='input-group date start' id='{{$start}}'>
                                        {!!Form::text('start', old('start'), ['class'=>'form-control required'])!!}
                                        <span class="input-group-addon"><span
                                                    class="glyphicon glyphicon-calendar"></span></span>
                                        @if ($errors->has('start'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('event_start') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class='input-group date' id='{{$end}}'>
                                        {!!Form::text('end', old('event_end'), ['class'=>'form-control required'])!!}
                                        <span class="input-group-addon"><span
                                                    class="glyphicon glyphicon-calendar"></span></span>
                                        @if ($errors->has('end'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('end') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                {!! Form::button('Подтвердить', ['type'=>'submit', 'class'=>'btn btn-primary'])!!}
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>