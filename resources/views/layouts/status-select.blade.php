@extends('layouts.modal')

@section($modal_id)
    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}  col-lg-12">
        {!!Form::label('status', 'Статус:', ['class'=>'col-md-4 control-label class add-candidates-modal'])!!}
        <div class="col-md-8 users-select">
            {!! Form::select('status', $statuses, null, ['class'=>'form-control']) !!}
            @if ($errors->has('status'))
                <span class="help-block">
                    <strong>{{ $errors->first('status') }}</strong>
                </span>
            @endif
        </div>
    </div>
@endsection