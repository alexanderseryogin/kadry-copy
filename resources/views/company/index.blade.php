@extends('layouts.app')

@section('content')
    <div class="row">
        @if(session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
        @endif
    </div>

    {{--<div class="row search-bar">--}}
        {{--<div class="container-fluid">--}}
            {{--<div class="col-lg-10 col-lg-offset-1">--}}
                {{--<div class="input-group">--}}
                    {{--<span class="input-group-btn">--}}
                        {{--<a class="btn btn-info" href="#">--}}
                            {{--<i class="glyphicon glyphicon-search"></i>--}}
                        {{--</a>--}}
                    {{--</span>--}}
                    {{--<input class="form-control" placeholder="Search for..." type="text">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="row">
        <div class="container-fluid">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="items-list">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th><i class="fa fa-copyright" aria-hidden="true"></i> Компания</th>
                            <th><i class="fa fa-home" aria-hidden="true"></i> Home page</th>
                            <th><i class="fa fa-building" aria-hidden="true"></i> Регион</th>
                            <th><i class="fa fa-info-circle" aria-hidden="true"></i> Info</th>
                            @can('update', new \App\Company)
                                <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                            @endcan
                            @can('delete', new \App\Company)
                                <th><i class="fa fa-trash" aria-hidden="true"></i></th>
                            @endcan
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($companies as $company)
                            <tr>
                                <td>
                                    <a class="item" id="vacancy-title" href="/company/{{$company->id}}/">
                                        {{$company->title}}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{$company->link}}">{{$company->link}}</a>
                                </td>
                                <td>{{$company->dictionaryValue('location')}}</td>
                                <td class="company-info"><i class="fa fa-info-circle"
                                                            aria-hidden="true"></i> {{$company->info}}</td>
                                @can('update', $company)
                                    <td>
                                        {{Form::open([ 'method'  => 'get', 'route' => [ 'company.edit', $company->id ]])}}
                                        {!!Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                                        {!!Form::close()!!}
                                    </td>
                                @endcan
                                @can('delete', $company)
                                    <td>
                                        {{Form::open([ 'method'  => 'delete', 'class'=>'delete-form', 'route' => [ 'company.destroy', $company->id ]])}}
                                        {!!Form::button('<i class="fa fa-btn fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                                        {!!Form::close()!!}
                                    </td>
                                @endcan
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/confirm_delete.js')}}"></script>
@endsection