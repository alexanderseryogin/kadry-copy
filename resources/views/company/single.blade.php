@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-primary add-shadows">
            <div class="panel-heading">
                Компания № {{$company->id}}
                <div class="col-md-1 user-controls pull-right">
                    @can('update', $company)
                        {{Form::open([ 'method'  => 'get', 'route' => [ 'company.edit', $company->id ]])}}
                        {!!Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                        {!!Form::close()!!}
                    @endcan

                    @can('delete', $company)
                        {{Form::open([ 'method'  => 'delete', 'route' => [ 'company.destroy', $company->id ]])}}
                        {!!Form::button('<i class="fa fa-btn fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                        {!!Form::close()!!}
                    @endcan
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-stripped">
                    <tr>
                        <th>Вакансия</th>
                        <th>Домашняя страница</th>
                        <th>Регион</th>
                    </tr>
                    <tr>
                        <td>
                            <a id="vacancy-title" href="/vacancy/{{$company->id}}/">
                                {{$company->title}}
                            </a>
                        </td>
                        <td>{{$company->link}}</td>
                        <td>{{$company->dictionaryValue('location')}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-info-circle" aria-hidden="true"></i> {{$company->info}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection