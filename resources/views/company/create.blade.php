@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary add-shadows">
                    <div class="panel-heading">Управление компанией:</div>
                    <div class="panel-body">
                        {!!Form::model($company,['action'=>[$action, $company->id], 'method'=>$method, 'class'=>'form-horizontal'])!!}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            {!!Form::label('title', 'Название компании: ', ['class'=>'col-md-4 control-label required'])!!}
                            <div class="col-md-6">
                                {!!Form::text('title', old('title'), ['class'=>'form-control required'])!!}
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
                            {!!Form::label('link', 'Домашняя страница: ', ['class'=>'col-md-4 control-label'])!!}
                            <div class="col-md-6">
                                {!!Form::text('link', old('link'), ['class'=>'form-control required'])!!}
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('link') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
                            {!!Form::label('location_id', 'Регион: ', ['class'=>'col-md-4 control-label requierd'])!!}
                            <div class="col-md-6">
                                {!!Form::select('location_id', $locations, old('location_id'), ['class'=>'form-control'])!!}
                                @if ($errors->has('location_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('location_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('info') ? ' has-error' : '' }}">
                            {!!Form::label('info', 'О компании: ', ['class'=>'col-md-4 control-label requierd'])!!}
                            <div class="col-md-6">
                                {!!Form::textarea('info', old('info'), ['class'=>'form-control'])!!}
                                @if ($errors->has('info'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('info') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{route('companies')}}" class="btn btn-default">
                                    <i class="fa fa-backward" aria-hidden="true"></i> Назад
                                </a>
                                {!!Form::button('<i class="fa fa-btn fa-user"></i> Сохранить', ['type'=>'submit', 'class'=>'btn btn-primary pull-right'])!!}
                            </div>
                        </div>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
