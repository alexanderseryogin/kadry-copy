@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{!! asset('css/bootstrap-datetimepicker.min.css') !!}">
@endsection

@section('content')
    <div class="container">
        @if(count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $e)
                        <li>{{$e}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @include('layouts.user-create')
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{!! asset('js/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('js/bootstrap-datetimepicker.min.js') !!}"></script>
    <script src="{!! asset('js/init-birthday.js') !!}"></script>
@endsection