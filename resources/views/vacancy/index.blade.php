@extends('layouts.app')

@section('content')
    <div class="row">
        @if(session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
        @endif
    </div>

    {{--<div class="row  search-bar">--}}
        {{--<div class="container-fluid">--}}
            {{--<div class="col-lg-10 col-lg-offset-1">--}}
                {{--<div class="input-group">--}}
                    {{--<span class="input-group-btn">--}}
                        {{--<a class="btn btn-info" href="#">--}}
                            {{--<i class="glyphicon glyphicon-search"></i>--}}
                        {{--</a>--}}
                    {{--</span>--}}
                    {{--<input class="form-control" placeholder="Search for..." type="text">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="row">
        <div class="container-fluid">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="items-list">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Вакансия</th>
                            <th>З/П</th>
                            <th>Регион</th>
                            <th>Требования</th>
                            @can('update', new \App\Vacancy)
                                <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                            @endcan
                            @can('delete', new \App\Vacancy())
                                <th><i class="fa fa-trash" aria-hidden="true"></i></th>
                            @endcan
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($vacancies as $vacancy)
                            <tr>
                                <td>
                                    <a class="item" id="vacancy-title"
                                       href="{{route('vacancy.show', ['id' => $vacancy->id])}}">
                                        {{$vacancy->title}}
                                    </a>
                                </td>
                                <td>{{$vacancy->salary}} {{$vacancy->dictionaryValue('currency')}}</td>
                                <td>{{$vacancy->dictionaryValue('location')}}</td>
                                <td>{{$vacancy->requirements}}</td>
                                @can('update', $vacancy)
                                    <td>
                                        {{Form::open([ 'method'  => 'get', 'route' => [ 'vacancy.edit', $vacancy->id ]])}}
                                        {!!Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                                        {!!Form::close()!!}
                                    </td>
                                @endcan
                                @can('delete', new $vacancy)
                                    <td>
                                        {{Form::open([ 'method'  => 'delete', 'class'=>'delete-form', 'route' => [ 'vacancy.destroy', $vacancy->id ]])}}
                                        {!!Form::button('<i class="fa fa-btn fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                                        {!!Form::close()!!}
                                    </td>
                                @endcan
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/confirm_delete.js')}}"></script>
@endsection