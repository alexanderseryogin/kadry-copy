@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="{!! asset('css/bootstrap-datetimepicker.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap-select.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap-colorpicker.min.css') !!}">
@endsection


@section('content')

    <div class="container-fluid">
        <div class="col-md-5">
            <div class="panel panel-primary add-shadows">
                <div class="panel-heading">
                    Вакансия № {{$vacancy->id}}
                    <div class="col-md-1 user-controls pull-right">
                        @can('update', $vacancy)
                            {{Form::open([ 'method'  => 'get', 'route' => [ 'vacancy.edit', $vacancy->id ]])}}
                            {!!Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                            {!!Form::close()!!}
                        @endcan
                        @can('delete', $vacancy)
                            {{Form::open([ 'method'  => 'delete', 'route' => [ 'vacancy.destroy', $vacancy->id ]])}}
                            {!!Form::button('<i class="fa fa-btn fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                            {!!Form::close()!!}
                        @endcan
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-stripped">
                        <tr>
                            <th class="text-right">Вакансия:</th>
                            <td>
                                <a id="vacancy-title" href="/vacancy/{{$vacancy->id}}/">
                                    {{$vacancy->title}}
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">З/П:</th>
                            <td>{{$vacancy->salary}} {{$vacancy->dictionaryValue('currency')}}</td>
                        </tr>
                        <tr>
                            <th class="text-right">Регион:</th>
                            <td>{{$vacancy->dictionaryValue('location')}}</td>
                        </tr>
                        <tr>
                            <th class="text-right">Требования:</th>
                            <td>{{$vacancy->requirements}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            @can('create', new \App\Candidate)
                <div class="add-candidates">
                    <a type="button" class="btn btn-success center-block user-single-button add-shadows"
                       data-toggle="modal"
                       data-target="#candidate-select">
                        Подобор кандидатов
                    </a>
                </div>
            @endcan

        </div>
        <div class="col-md-7">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                @foreach($statuses as $key=>$status)
                    <li class="nav-item">
                        <a class="nav-link {{!$key ? 'active':''}} {{\App\Candidate::getCssClass($key)}}"
                           data-toggle="tab" href="#status-{{$key}}" role="tab">{{$status}}</a>
                    </li>
                @endforeach
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                @foreach($statuses as $key=>$status)
                    <div class="tab-pane {{!$key ? 'active':''}} items-list candidates" id="status-{{$key}}"
                         role="tabpanel">
                        <table class="table table-bordered table-hover candidates-table">
                            <thead>
                            <tr>
                                <th>Имя</th>
                                <th>Зарплата</th>
                                <th>Перемещен</th>
                                <th>Статус</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($selected[$key]))
                                @foreach($selected[$key]['candidates'] as $candidate)
                                    @can('read', $candidate)
                                        <tr id="candidate-{{$candidate->id}}">
                                            <td>
                                                <a id="profile-name"
                                                   href="{{route('user.show', ['id'=>$candidate->user->id])}}">
                                                    {{$candidate->user->last_name . ' ' . $candidate->user->first_name}}
                                                </a>
                                            </td>
                                            <td class="text-center">{{$candidate->user->salary . ' ' . $candidate->user->dictionaryValue('currency')}}</td>
                                            <td class="text-center"><span
                                                        class="label label-info">{{$candidate->updated_at->format('d.m.y в H:i:s')}}</span>
                                            </td>
                                            <td class="text-center">
                                                @can('update', $candidate)
                                                    <button class="btn btn-xs status-label {{\App\Candidate::getCssClass($candidate->status)}}"
                                                            data-status="{{$candidate->status}}">
                                                        {{\App\Candidate::getStatus()[$candidate->status]}}
                                                    </button>
                                                @endcan

                                                @cannot('update', $candidate)
                                                    <label class="btn btn-xs status-label {{\App\Candidate::getCssClass($candidate->status)}}">
                                                        {{\App\Candidate::getStatus()[$candidate->status]}}
                                                    </label>
                                                @endcannot
                                            </td>
                                        </tr>
                                    @endcan
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        @if(!isset($selected[$key]))
                            <div class="empty">
                                <div class="text-center">Ни один кандидат не был выбран для данной категории</div>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>

    </div>
    <div class="row">
        <div class="container-fluid">
            <div class="row">
                <h3 class="text-center">История изменений:</h3>
            </div>

            <div class="row">
                <div class="container items-list">
                    <table class="table table-striped table-responsive history add-shadows">
                        <thead>
                        <th>Ответственный</th>
                        <th>Действие</th>
                        <th>Кандидат</th>
                        <th>Старый статус</th>
                        <th>Новый статус</th>
                        <th class="text-right">Дата</th>
                        </thead>
                        <tbody id="history_entries">

                        </tbody>
                    </table>

                    <div class="container pagination-container"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal For Candidate Select-->
    @can('create', new \App\Candidate)
        @include('layouts.candidate-select', [
        'modal_id'=>'candidate-select',
        'modal_title'=>'Выберите из списка кандидата:',
        'action'=>'CandidateController@store',
        'action_id'=>null,
        'method'=>'post',
        'start'=>'start-1',
        'end'=>'end-1',
        'color'=>'event-color-1'
        ])
    @endcan

    @can('update', new \App\Candidate)
        <!-- Modal For Status Change-->
        @include('layouts.status-select', [
        'modal_id'=>'status-select',
        'modal_title'=>'Изменить статус для кандидата',
        'action'=>'CandidateController@update',
        'action_id'=>0,
        'method'=>'put',
        'start'=>'start-2',
        'end'=>'end-2',
        'color'=>'event-color-2'
        ])
    @endcan


@endsection

@section('scripts')
    @can('create', new \App\Candidate)
        <script src="{{asset('js/vacancy_candidate_ajax.js')}}"></script>
        <script src="{!! asset('js/moment-with-locales.min.js') !!}"></script>
        <script src="{!! asset('js/bootstrap-datetimepicker.min.js') !!}"></script>
        <script src="{!! asset('js/bootstrap-select.min.js') !!}"></script>
        <script src="{!! asset('js/bootstrap-colorpicker.min.js') !!}"></script>
        <script src="{!! asset('js/event_create.js') !!}"></script>
    @endcan
    <script src="{!! asset('js/single_history.js') !!}"></script>
@endsection