@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary add-shadows">
                    <div class="panel-heading">Управление вакансией:</div>
                    <div class="panel-body">
                        {!!Form::model($vacancy,['action'=>[$action, $vacancy->id], 'method'=>$method, 'class'=>'form-horizontal'])!!}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            {!!Form::label('title', 'Вакансия: ', ['class'=>'col-md-4 control-label required'])!!}
                            <div class="col-md-6">
                                {!!Form::text('title', old('title'), ['class'=>'form-control required'])!!}
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('salary') ? ' has-error' : '' }}">
                            {!!Form::label('salary', 'Зарплата: ', ['class'=>'col-md-4 control-label requierd'])!!}
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-7">
                                        {!!Form::number('salary', old('salary'), ['class'=>'form-control'])!!}
                                    </div>
                                    <div class="col-md-5">
                                        {!!Form::select('currency_id', $currencies, old('currency'), ['class'=>'form-control'])!!}
                                    </div>
                                </div>
                                @if ($errors->has('salary'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('salary') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
                            {!!Form::label('location_id', 'Регион: ', ['class'=>'col-md-4 control-label requierd'])!!}
                            <div class="col-md-6">
                                {!!Form::select('location_id', $locations, old('location_id'), ['class'=>'form-control'])!!}
                                @if ($errors->has('location_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('location_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('lang') ? ' has-error' : '' }}">
                            {!!Form::label('lang', 'Языки: ', ['class'=>'col-md-4 control-label requierd'])!!}
                            <div class="col-md-6">
                                {!!Form::text('lang', old('lang'), ['class'=>'form-control'])!!}
                                @if ($errors->has('lang'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lang') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('requirements') ? ' has-error' : '' }}">
                            {!!Form::label('requirements', 'Описание: ', ['class'=>'col-md-4 control-label requierd'])!!}
                            <div class="col-md-6">
                                {!!Form::textarea('requirements', old('requirements'), ['class'=>'form-control'])!!}
                                @if ($errors->has('requirements'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('requirements') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{route('vacancies')}}" class="btn btn-default">
                                    <i class="fa fa-backward" aria-hidden="true"></i> Назад
                                </a>
                                {!!Form::button('<i class="fa fa-btn fa-user"></i> Создать', ['type'=>'submit', 'class'=>'btn btn-primary pull-right'])!!}
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
