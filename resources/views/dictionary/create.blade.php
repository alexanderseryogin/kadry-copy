@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary add-shadows">
                    <div class="panel-heading">Добавление записи в словарь:</div>
                    <div class="panel-body">
                        {!!Form::model($item, ['action'=>[$action, $item->id], 'method'=>'post', 'class'=>'form-horizontal'])!!}

                        <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
                            {!!Form::label('salary', 'Значение: ', ['class'=>'col-md-4 control-label requierd'])!!}
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        {!!Form::text('value', old('value'), ['class'=>'form-control'])!!}
                                    </div>
                                    <div class="col-md-6">
                                        {!!Form::select('type', $types, old('category'), ['class'=>'form-control', 'id'=>'type'])!!}
                                    </div>
                                </div>
                                @if ($errors->has('salary'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{route('dictionary.index')}}" class="btn btn-default">
                                    <i class="fa fa-backward" aria-hidden="true"></i> Назад
                                </a>
                                {!!Form::button('<i class="fa fa-btn fa-user"></i> Создать', ['type'=>'submit', 'class'=>'btn btn-primary pull-right'])!!}
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/dictionary.js')}}"></script>
@endsection
