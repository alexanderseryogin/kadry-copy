@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-primary add-shadows">
            <div class="panel-heading">
                Запись № {{$item->id}}
                <div class="col-md-1 user-controls pull-right">
                    @can('update', new \App\Dictionary)
                        {{Form::open([ 'method'  => 'get', 'route' => [ 'dictionary.edit', $item->id ]])}}
                        {!!Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                        {!!Form::close()!!}
                    @endcan

                    @can('delete', new \App\Dictionary)
                        {{Form::open([ 'method'  => 'delete', 'route' => [ 'dictionary.destroy', $item->id ]])}}
                        {!!Form::button('<i class="fa fa-btn fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                        {!!Form::close()!!}
                    @endcan
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-stripped">
                    <tr>
                        <th>Тип</th>
                        <th>Значение</th>
                    </tr>
                    <tr>
                        <td>{{\App\Dictionary::getDictionaryTypes()[$item->type]}}</td>
                        <td>{{$item->value}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection