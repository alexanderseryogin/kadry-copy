@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary add-shadows">
                    <div class="panel-heading">Управление словарем:</div>
                    <div class="panel-body">
                        {!!Form::model($item,['action'=>['DictionaryController@update', $item->id], 'method'=>'put', 'class'=>'form-horizontal'])!!}

                        <div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
                            {!!Form::label('value', 'Значение: ', ['class'=>'col-md-4 control-label required'])!!}
                            <div class="col-md-6">
                                {!!Form::text('value', old('value'), ['class'=>'form-control required'])!!}
                                @if ($errors->has('value'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{route('dictionary.index')}}" class="btn btn-default">
                                    <i class="fa fa-backward" aria-hidden="true"></i> Назад
                                </a>
                                {!!Form::button('<i class="glyphicon glyphicon-refresh"></i> Обновить', ['type'=>'submit', 'class'=>'btn btn-primary pull-right'])!!}
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/dictionary.js')}}"></script>
@endsection