@extends('layouts.app')

@section('content')
    <div class="container">
        {!!Form::open(['action'=>['DictionaryController@toggle'], 'method'=>'get', 'class'=>'form-horizontal'])!!}
        <div class="form-group">
            {!!Form::label('category', 'Категория: ', ['class'=>'col-md-4 control-label'])!!}
            <div class="col-md-6">
                {!!Form::select('category', $categories, old('category'), ['class'=>'form-control'])!!}
            </div>
        </div>
        {!!Form::close()!!}
    </div>

    <div class="container">
        <div class="items-list">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    @can('update', new \App\Dictionary)
                        <th><i class="fa fa-pencil-square-o" aria-hidden="true"></i></th>
                    @endcan
                    @can('delete', new \App\Dictionary)
                        <th><i class="fa fa-trash" aria-hidden="true"></i></th>
                    @endcan
                </tr>
                </thead>
                <tbody id="items-list"></tbody>
            </table>
        </div>
    </div>

    <div class="container pagination-container"></div>
    <div class="center-text" id="loading-container">
        <img src="{{asset('icon/loading.gif')}}" alt="loading" class="loading">
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/dictionary.js')}}"></script>
    <script src="{{asset('js/confirm_delete.js')}}"></script>
@endsection

