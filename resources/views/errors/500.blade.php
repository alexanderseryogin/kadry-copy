@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="container-fluid error-img" style="background-image: url({{asset('icon/500_Error.png')}})"></div>
    </div>
    <div class="row">
        <div class="container error-message">
            <div class="alert alert-danger text-center">
                <strong>{{$exception->getMessage()}}</strong>
            </div>
        </div>
    </div>



@endsection