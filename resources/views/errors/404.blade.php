@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="container-fluid error-img" style="background-image: url({{asset('icon/404_Error.png')}})"></div>
    </div>
@endsection

