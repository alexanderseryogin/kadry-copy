<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Untitled Document</title>
</head>
<body>

<div style="width:100%;" align="center">

    <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" valign="top" style="background-color:rgba(10, 20, 32, 0.93);" bgcolor="#53636e;">

                <br>
                <br>
                <table width="583" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="left" valign="top" bgcolor="#32506e" style="background-color:#32506e;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="35">&nbsp;</td>
                                    <td height="50" valign="middle"
                                        style="color:#FFFFFF; font-size:11px; font-family:Arial, Helvetica, sans-serif; text-align: center">
                                        <b>Напоминание о предстоящем событии</b><br>
                                    </td>
                                    <td width="35">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" bgcolor="#FFFFFF" style="background-color:#FFFFFF;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="35" align="left" valign="top">&nbsp;</td>
                                    <td align="left" valign="top">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center" valign="top">
                                                    <br><br>
                                                    <div style="color:#32506e; font-family:Times New Roman, Times, serif; font-size:28px;">
                                                        {{$event->title}}
                                                    </div>
                                                    <br>
                                                    <div style="font-family: Verdana, Geneva, sans-serif; color:#32506e; font-size:12px; font-weight: bold">
                                                        {{$event->start->format('H:i')}}
                                                        - {{$event->end->format('H:i d.m.Y')}}
                                                    </div>
                                                    <br>
                                                </td>
                                            </tr>
                                            {{--<tr>--}}
                                            {{--<td align="left" valign="top">--}}
                                            {{--<img src="{{asset('emails/img/pic1.jpg')}}" width="512" height="296" vspace="10">--}}
                                            {{--<img src="https://www.dropbox.com/s/amx7m7urh6qhx5c/pic1.jpg?dl=0"--}}
                                            {{--width="512" height="296" vspace="10">--}}

                                            {{--</td>--}}
                                            {{--</tr>--}}
                                            <tr>
                                                <td align="left" valign="top"
                                                    style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#525252; text-align: justify;">
                                                    <br>
                                                    <div style="color:#32506e; font-size:19px; text-align: center">
                                                        Уважаемый(-ая) {{$user->first_name}} {{$user->patronymic}},
                                                    </div>
                                                    <br><br><br>
                                                    <div style="color:#32506e;">
                                                        Вы получили это сообщене, потому что являетесь участником
                                                        события "{{$event->title}}"
                                                    </div>
                                                    <br><br><br>
                                                    <b>Описание события:</b><br><br>
                                                    {{$event->description}}<br><br><br>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <th>Участники:</th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <ul>
                                                                    @foreach($event->participants as $participant)
                                                                        <li>{{$participant->user->last_name}} {{$participant->user->first_name}}</li>
                                                                    @endforeach
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top"
                                                    style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#525252;">
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="35" align="left" valign="top">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" bgcolor="#32506e" style="background-color:#32506e;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="35">&nbsp;</td>
                                    <td height="50" valign="middle"
                                        style="color:#FFFFFF; font-size:11px; font-family:Arial, Helvetica, sans-serif;">
                                        <b>FaceIT HRM</b><br>
                                        {{Carbon\Carbon::now()->format('H:i d.m.Y')}}
                                    </td>
                                    <td width="35">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br><br></td>
        </tr>
    </table>
</div>
</body>
</html>