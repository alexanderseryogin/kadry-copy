@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-primary add-shadows">
            <div class="panel-heading">
                <i class="fa fa-circle" aria-hidden="true"
                   style="color: {{$event->color}}"></i>&nbsp;&nbsp;&nbsp;{{$event->title}}
                ( {{$event->start->format('d.m H:i')}} - {{$event->end->format('d.m H:i')}} )
                <div class="col-md-1 user-controls pull-right">
                    @can('update', $event)
                        {{Form::open([ 'method'  => 'get', 'route' => [ 'event.edit', $event->id ]])}}
                        {!!Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                        {!!Form::close()!!}
                    @endcan
                    @can('delete', $event)
                        {{Form::open([ 'method'  => 'delete', 'route' => [ 'event.destroy', $event->id ]])}}
                        {!!Form::button('<i class="fa fa-btn fa-trash"></i>', ['type'=>'submit', 'class'=>'btn btn-xs user-control'])!!}
                        {!!Form::close()!!}
                    @endcan
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-stripped">
                    <tr>
                        <th>Менеджер:</th>
                        <td>{{$event->manager->last_name}} {{$event->manager->first_name}}</td>
                    </tr>
                    <tr>
                        <th>Описание:</th>
                        <td>{{$event->description}}</td>
                    </tr>
                    <tr>
                        <th>Участники:</th>
                        <td>
                            <ul>
                                @foreach($event->participants as $participant)
                                    <li>
                                        {{$participant->user->last_name}} {{$participant->user->first_name}}
                                    </li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
@endsection