@extends('layouts.app')
@section('styles')
    <link href="https://fonts.googleapis.com/css?family=Marck+Script" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-md-5">
            <h4 class="text-center">Предстоящие события:</h4>
            <div class="items-list">
                <table class="table table-striped table-responsive add-shadows" id="upcoming-events">
                    <thead>
                    <th>Тип события</th>
                    <th>Описание</th>
                    <th class="text-right">Дата</th>
                    </thead>
                    <tbody>
                    <tr>
                        <th colspan="3" class="text-center">
                            <img src="{{asset('icon/loading.gif')}}" alt="loading" class="loading">
                        </th>
                    </tr>
                    </tbody>
                </table>
                <div class="container" id="events-pagination"></div>
            </div>
        </div>
        <div class="col-md-7 ">
            <h4 class="text-center">Стикеры:</h4>
            <div id="stickers-container">
                <div class="pull-left" style="padding-right: 15px;">
                    <div class="panel add-shadows" id="stickers-menu">
                        <div class="panel-body" style="min-height: 125px; width: 223px;">
                            <button class="btn btn-success btn-block add-shadows" id="add-sticker" data-toggle="modal"
                                    data-target="#add-sticker-modal">Добавить стикер
                            </button>
                            <button class="btn btn-warning btn-block add-shadows active-stickers" id="toggle-stickers">
                                Закрытые стикеры
                            </button>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="container pagination-container" id="stickers-pagination"></div>
                </div>
            </div>
        </div>
    </div>


    </div>
    <div class="container-fluid">
        <h4 class="text-center">Последние действия на сайте:</h4>
        <div class="container-fluid items-list">
            <table class="table table-striped table-responsive history add-shadows">
                <thead>
                <th>Ответственный</th>
                <th>Действие</th>
                <th>Кандидат</th>
                <th>Вакансия</th>
                <th>Старый статус</th>
                <th>Новый статус</th>
                <th class="text-right">Дата</th>
                </thead>
                <tbody id="history_entries">
                <tr>
                    <th colspan="7" class="text-center">
                        <img src="{{asset('icon/loading.gif')}}" alt="loading" class="loading">
                    </th>
                </tr>
                </tbody>
            </table>
            <div class="container pagination-container" id="actions-pagination"></div>
        </div>

    </div>

    <div class="modal fade" id="add-sticker-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Добавить стикер</h4>
                </div>

                {!! Form::open(['action'=>['OrganizerController@stickerSave'], 'method'=>'post', 'class'=>'modal-form', 'id'=>'sticker-form'])!!}

                <div class="modal-body">
                    <div class="row">
                        <div class="form-group {{ $errors->has('sticker_text') ? ' has-error' : '' }}  col-lg-12">
                            {!!Form::label('sticker_text', 'Текст стикера', ['class'=>'col-md-4 control-label class'])!!}
                            <div class="col-md-8 users-select">
                                {!!Form::textarea('sticker_text', null, ['class'=>'form-control', 'cols'=>'50', 'rows'=>'3','style'=>'resize: none'])!!}
                                Осталось символов: <span id="charcounter"></span>
                                @if ($errors->has('sticker_text'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('sticker_text') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        @can('assign', new \App\Sticker)
                            <div class="form-group{{ $errors->has('addressee_id') ? ' has-error' : '' }}  col-lg-12">
                                {!!Form::label('addressee_id', 'Адресат:', ['class'=>'col-md-4 control-label class add-candidates-modal'])!!}
                                <div class="col-md-8 users-select">
                                    {!!Form::select('addressee_id', $users, Auth::user()->id, ['class'=>'form-control'])!!}
                                    @if ($errors->has('addressee_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('addressee_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        @endcan
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                    {!! Form::button('Подтвердить', ['type'=>'submit', 'class'=>'btn btn-primary'])!!}
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Вы действительно хотите удалить стикер?
                </div>
                <div class="modal-body" id="sticker-content">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <a class="btn btn-danger btn-ok">Удалить</a>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script src="{!! asset('js/upcoming_events.js') !!}"></script>
    <script src="{!! asset('js/general_history.js') !!}"></script>
    <script src="{!! asset('js/stickers_ajax.js') !!}"></script>
@endsection