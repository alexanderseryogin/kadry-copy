@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="{!! asset('css/bootstrap-datetimepicker.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap-select.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap-colorpicker.min.css') !!}">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Создание события</div>
                    <div class="panel-body">
                        {!!Form::model($event,['action'=>[$action, $event->id], 'method'=>$method,'class'=>'form-horizontal'])!!}

                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            {!!Form::label('type', 'Тип события: ', ['class'=>'col-md-4 control-label'])!!}
                            <div class="col-md-6">
                                {!!Form::select('type', $types, old('type'), ['class'=>'form-control'])!!}
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}" id="title-div">
                            {!!Form::label('title', 'Заголовок: ', ['class'=>'col-md-4 control-label'])!!}
                            <div class="col-md-6">
                                {!!Form::text('title', old('title'), ['class'=>'form-control required'])!!}
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('color') ? ' has-error' : '' }}">
                            {!!Form::label('color', 'Цвет события: ', ['class'=>'col-md-4 control-label'])!!}
                            <div class="col-md-6">
                                <div id="event-color-1" class="input-group colorpicker-component">
                                    {!!Form::text('color', old('color')?old('color'):'#3399f3', ['class'=>'form-control'])!!}
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                                @if ($errors->has('color'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('color') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            {!!Form::label('description', 'Описание: ', ['class'=>'col-md-4 control-label'])!!}
                            <div class="col-md-6">
                                {!!Form::textarea('description', old('description'), ['class'=>'form-control'])!!}
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @can('create', new \App\Participant)
                        <div class="form-group{{ $errors->has('participants') ? ' has-error' : '' }}">
                            {!!Form::label('participants', 'Участники: ', ['class'=>'col-md-4 control-label'])!!}
                            <div class="col-md-6">
                                {!!Form::select('participants[]', $participants, $selected, ['class'=>'form-control selectpicker', 'multiple' => 'multiple', "data-live-search"=>"true"])!!}
                                @if ($errors->has('participants'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('participants') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        @endcan


                        <div class="form-group{{ $errors->has('start') || $errors->has('end') ? ' has-error' : '' }}">
                            {!!Form::label('start', 'Время: ', ['class'=>'col-md-4 control-label requierd'])!!}
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class='input-group date' id='start-1'>
                                            {!!Form::text('start', old('start'), ['class'=>'form-control required'])!!}
                                            <span class="input-group-addon"><span
                                                        class="glyphicon glyphicon-calendar"></span></span>
                                            @if ($errors->has('start'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('start') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class='input-group date' id='end-1'>
                                            {!!Form::text('end', old('end'), ['class'=>'form-control required'])!!}
                                            <span class="input-group-addon"><span
                                                        class="glyphicon glyphicon-calendar"></span></span>
                                            @if ($errors->has('end'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('end') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{route('events')}}" class="btn btn-default">
                                    <i class="fa fa-backward" aria-hidden="true"></i> Назад
                                </a>
                                {!!Form::button('<i class="fa fa-btn fa-user"></i> Создать', ['type'=>'submit', 'class'=>'btn btn-primary pull-right'])!!}
                            </div>
                        </div>

                        {!!Form::close()!!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{!! asset('js/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('js/bootstrap-datetimepicker.min.js') !!}"></script>
    <script src="{!! asset('js/bootstrap-select.min.js') !!}"></script>
    <script src="{!! asset('js/bootstrap-colorpicker.min.js') !!}"></script>
    <script src="{!! asset('js/event_create.js') !!}"></script>
@endsection