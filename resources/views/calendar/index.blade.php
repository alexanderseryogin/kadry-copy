@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="{!! asset('css/fullcalendar.min.css') !!}">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-md-6 col-md-offset-3">
            <div class="pull-right">
                <a href="{{route('event.create')}}" class="btn btn-primary add-shadows" id="create-event">Добавить событие</a>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="col-md-6 col-md-offset-3">
            {!! $calendar->calendar() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{!! asset('js/moment-with-locales.min.js') !!}"></script>
    <script src="{!! asset('js/fullcalendar.min.js') !!}"></script>
    {!! $calendar->script() !!}
@endsection
