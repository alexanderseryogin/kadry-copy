$(function () {
    var url = window.location.href + '/history';
    sendAjax(url, null, $('meta[name="page"]').attr('content'));
});

function sendAjax(url, dataString, column) {
    var loading = $($('<tr>')
        .append(
            $('<th>').attr('colspan', '6').addClass('text-center')
                .append($('<img>').addClass('loading').attr('src', '../icon/loading.gif'))
        ));
    $('tbody#history_entries').html('').append(loading);

    $.ajax({
        type: "GET",
        url: url,
        data: dataString,
        success: function (response) {
            console.log(response);

            //очищаю таблицу
            $('tbody#history_entries').html('');
            //
            // // подставляю ссылки
            $('div.pagination-container').html(response.links);
            //
            // // вешаю события на ссылки
            $('ul.pagination a').on('click', function (e) {
                e.preventDefault();
                var url = window.location.href + '/history';
                var dataString = 'page=' + $(this).attr('href').split('page=')[1];
                sendAjax(url, dataString, column);
            });

            for (var i in response.data) {
                var text = '';
                if (column == 'user') {
                    text = response.data[i].vacancy;
                } else if (column == 'vacancy') {
                    text = response.data[i].user;
                }

                $('tbody#history_entries').append(
                    $('<tr>').append(
                        $('<td>').text(response.data[i].manager)
                    ).append(
                        $('<td>').addClass('text-center').text(response.data[i].action)
                    ).append(
                        $('<td>').addClass('text-center').text(text)
                    ).append(
                        $('<td>').addClass('text-center').append(
                            $('<span>').addClass('label ' + response.data[i].old_class).text(response.data[i].old_val
                            ))
                    ).append(
                        $('<td>').addClass('text-center').append(
                            $('<span>').addClass('label ' + response.data[i].new_class).text(response.data[i].new_val
                            ))
                    ).append(
                        $('<td>').append(
                            $('<div>').addClass('label label-info add-shadows').text(response.data[i].date)
                        ))
                );
            }
        }
    });
}