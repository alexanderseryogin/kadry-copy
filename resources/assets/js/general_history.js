$(function () {
    var url = window.location.href + '/history';
    sendAjax(url, null);
});

function sendAjax(url, dataString) {
    $.ajax({
        type: "GET",
        url: url,
        data: dataString,
        success: function (response) {
            console.log(response);

            //очищаю таблицу
            $('tbody#history_entries').html('');
            //
            // // подставляю ссылки
            $('div.pagination-container#actions-pagination').html(response.links);
            //
            // // вешаю события на ссылки
            $('#actions-pagination ul.pagination a').on('click', function (e) {
                e.preventDefault();
                var url = window.location.href + '/history';
                var dataString = 'page=' + $(this).attr('href').split('page=')[1];
                sendAjax(url, dataString);
            });

            for (var i in response.data) {
                $('tbody#history_entries').append(
                    $('<tr>').append(
                        $('<td>').text(response.data[i].manager)
                    ).append(
                        $('<td>').addClass('text-center').text(response.data[i].action)
                    ).append(
                        $('<td>').addClass('text-center').text(response.data[i].user)
                    ).append(
                        $('<td>').addClass('text-center').text(response.data[i].vacancy)
                    ).append(
                        $('<td>').addClass('text-center').append(
                            $('<span>').addClass('label ' + response.data[i].old_class).text(response.data[i].old_val
                            ))
                    ).append(
                        $('<td>').addClass('text-center').append(
                            $('<span>').addClass('label ' + response.data[i].new_class).text(response.data[i].new_val
                            ))
                    ).append(
                        $('<td>').append(
                            $('<div>').addClass('label label-info add-shadows').text(response.data[i].date)
                        ))
                );
            }
        }
    });
}