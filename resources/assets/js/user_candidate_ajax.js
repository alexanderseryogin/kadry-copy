$(function () {

    // display change-status modal
    $('button.status-label').on('click', function () {
        statusEvent($(this));
    });

    // display interview appointment
    $('select#status').on('change', function () {
        if ($(this).val() == 3) {
            $('div#status-select div.inteview-checker').slideDown();
        } else {
            $('div#status-select #interview').prop("checked", false);
            $('div#status-select div.inteview-checker').slideUp();
            $(this).parents('div.modal-body').find('div.interview-row').slideUp();
        }
    });

    // display event fields
    $('.appoint-checkbox').on('change', function () {
        $(this).parents('div.modal-body').find('div.interview-row').slideToggle();
    });

    // modal form ajax submit
    $('.modal-form').on('submit', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).data('method'),
            data: $(this).serialize(),
            success: function (response) {
                sendAjax(window.location.href + '/history', null, $('meta[name="page"]').attr('content'));
                if (response.action == 'added') {
                    addVacancyRow(response);
                } else if (response.action == 'updated') {
                    changeStatusButton(response);
                }
            },
            error: function (response) {
                console.log(response.responseText);
                var errors = $.parseJSON(response.responseText);
                $.each(errors, function (key) {
                    $('[name="' + key + '"]').parents('div.form-group').addClass('has-error');
                });
            }
        });
    });

    // clear errors fields
    $('#status-select, #vacancy-select, #candidate-select').on('hidden.bs.modal', function () {
        $('div.form-group').removeClass('has-error');
        $('div#status-select div.inteview-checker').slideUp();
        $(this).find('div.interview-row').slideUp();
        $(this).find('#interview').prop("checked", false);
    });

    // deletes vacancy from vacancies table
    $('.delete-form').on('submit', function (e) {
        e.preventDefault();
        e.stopPropagation();
        deleteAjax($(this));
    });

});

// adds vacancy on ajax success
function addVacancyRow(response) {
    var tr = $('<tr>').attr('id', "candidate-" + response.candidate_id)
        .append(
            $('<td>').append($('<a>').addClass('item')
                .attr('href', "/vacancy/" + response.vacancy_id + "/")
                .text(response.vacancy_title))
        ).append(
            $('<td class="text-center">').text(response.vacancy_salary)
        ).append(
            $('<td class="text-center">')
                .append(
                    $('<button>')
                        .addClass('btn btn-xs status-label ' + response.candidate_class)
                        .text(response.candidate_status)
                        .on('click', function () {
                            statusEvent($(this));
                        }).data('status', response.status_id))
        ).append(
            $('<td class="text-center">').append(
                $('<form method="POST" action="/candidate/' + response.candidate_id + '" accept-charset="UTF-8" class="delete-form"></form>')
                    .append('<input name="_method" type="hidden" value="DELETE">')
                    .append('<input name="_token" type="hidden" value="' + $('meta[name="csrf-token"]').attr('content') + '">')
                    .append('<button type="submit" class="btn btn-xs user-control"><i class="fa fa-btn fa-trash"></i></button>')
                    .on('submit', function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        deleteAjax($(this));
                    }))
        );

    $('.candidates-table').append(tr);

    $("option[value=" + response.vacancy_id + "]").remove();
    $('#vacancy-select').modal('hide');
}

// changes candidate status button
function changeStatusButton(response) {
    console.log(response);

    ($('tr#candidate-' + response.id).find('button.status-label'))
        .removeClass(response.old_class)
        .addClass(response.new_class)
        .text(response.candidate_status)
        .data('status', response.status);

    $('#status-select').modal('hide');
}

// deletes vacancy from candidate table
function deleteAjax(form) {
    if (!confirm("Подтвердить удаление " +
            form.parents('tr:first').find('a.item').text().trim() +
            "?"
        )) {
        return false;
    } else {
        $.ajax({
            url: form.attr('action'),
            type: 'DELETE',
            data: form.serialize(),
            success: function (response) {
                console.log(response);
                $('tr#candidate-' + response.candidate_id).remove();
                var options = $('select[name="vacancy_id"] option');
                var arr = options.map(function (_, option) {
                    return {text: $(option).text(), value: $(option).val()};
                }).get();

                arr.push({text: response.vacancy_title, value: response.vacancy_id});

                arr.sort(function (option1, option2) {
                    return option1.text > option2.text ? 1 : option1.text < option2.text ? -1 : 0;
                });
                options.each(function (i, option) {
                    option.value = arr[i].value;
                    $(option).text(arr[i].text);
                });
            }
        });
    }
}

// adds event listener on status button
function statusEvent(button) {
    $('select#status').val(button.data('status'));
    $('#status-select').find('form').attr('action', '/candidate/' + button.parents('tr').attr('id').split('candidate-')[1]);
    $('#status-select span.modal-item').text(button.parents('tr').find('td:first').text().trim() + '?');
    $('#status-select').modal('show');
}