$(function () {
    if (localStorage.selected) {
        $('#type').val(localStorage.selected);
    }

    $('#type').on('change', function () {
        localStorage.setItem('selected', $(this).val());
    });
});