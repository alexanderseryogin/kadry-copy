$(function () {
    var url = window.location.href + '/events';
    getEventsAjax(url, null);
});

function getEventsAjax(url, dataString) {
    $.ajax({
        type: "GET",
        data: dataString,
        url: url,
        success: function (response) {
            console.log(response);

            //очищаю таблицу
            $('#upcoming-events tbody').html('');
            //
            // // подставляю ссылки
            $('#events-pagination').html(response.links);
            //
            // // вешаю события на ссылки
            $('#events-pagination ul.pagination a').on('click', function (e) {
                e.preventDefault();
                var url = window.location.href + '/history';
                var dataString = 'page=' + $(this).attr('href').split('page=')[1];
                getEventsAjax(window.location.href + '/history');
            });

            for (var i in response.data) {
                $('#upcoming-events tbody').append(
                    $('<tr>').append(
                        $('<td>').addClass('text-center').append(
                            $('<a>').attr('href', '../event/'+response.data[i].id).css('text-decoration','none').append(
                                $('<span>').addClass('label label-info add-shadows')
                                    .css('background-color', response.data[i].color)
                                    .text(response.data[i].title)
                            )
                        )
                    ).append(
                        $('<td>').addClass('text-center').text(response.data[i].description)
                    ).append(
                        $('<td>').append(
                            $('<div>').addClass('label label-info add-shadows').text(response.data[i].date)
                        ))
                );
            }
        }
    });
}