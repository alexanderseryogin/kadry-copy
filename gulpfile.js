var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass('app.scss', 'public/css/app.css');
    mix.sass('bootstrap.min.scss', 'public/css/bootstrap.min.css');
    mix.sass('bootstrap.min.scss', 'public/css/bootstrap-theme.min.css');
    mix.sass('fullcalendar.min.scss', 'public/css/fullcalendar.min.css');
    mix.sass('bootstrap-datetimepicker.min.scss', 'public/css/bootstrap-datetimepicker.min.css');
    mix.sass('bootstrap-select.min.scss', 'public/css/bootstrap-select.min.css');
    mix.sass('bootstrap-colorpicker.min.scss', 'public/css/bootstrap-colorpicker.min.css');

    mix.scripts(['jquery.min.js'], 'public/js/jquery.min.js');
    mix.scripts(['bootstrap.min.js'], 'public/js/bootstrap.min.js');
    mix.scripts(['moment.min.js'], 'public/js/moment.min.js');
    mix.scripts(['moment-with-locales.min.js'], 'public/js/moment-with-locales.min.js');

    mix.scripts(['fullcalendar.min.js'], 'public/js/fullcalendar.min.js');
    mix.scripts(['bootstrap-datetimepicker.min.js'], 'public/js/bootstrap-datetimepicker.min.js');
    mix.scripts(['bootstrap-select.min.js'], 'public/js/bootstrap-select.min.js');
    mix.scripts(['bootstrap-colorpicker.min.js'], 'public/js/bootstrap-colorpicker.min.js');

    mix.scripts(['toggle.js', 'dictionary_pre_select.js'], 'public/js/dictionary.js');
    mix.scripts(['confirm_delete.js'], 'public/js/confirm_delete.js');
    mix.scripts(['user_candidate_ajax.js'], 'public/js/user_candidate_ajax.js');
    mix.scripts(['vacancy_candidate_ajax.js'], 'public/js/vacancy_candidate_ajax.js');
    mix.scripts(['event_create.js'], 'public/js/event_create.js');
    mix.scripts(['single_history.js'], 'public/js/single_history.js');
    mix.scripts(['general_history.js'], 'public/js/general_history.js');
    mix.scripts(['upcoming_events.js'], 'public/js/upcoming_events.js');
    mix.scripts(['stickers_ajax.js'], 'public/js/stickers_ajax.js');
    mix.scripts(['upload_cv.js'], 'public/js/upload_cv.js');
    mix.scripts(['parse_ajax.js'], 'public/js/parse_ajax.js');
    mix.scripts(['init-birthday.js'], 'public/js/init-birthday.js');
});
